<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->Common_Model = new Common_Model();
    }

    public function index()
    {
        $testimonials = $this->Common_Model->get_all('testimonials');
        $data = null;
        if($testimonials !=""){
            $data['testimonials'] = $testimonials;
        }
        $this->load->view('index',$data);
    }

    public function about()
    {
        $this->load->view('about');
    }

    public function pricing()
    {
        $this->load->view('pricing');
    }

    public function gallery()
    {
        $this->load->view('gallery');
    }

    public function maintenance()
    {
        $this->load->view('maintenance');
    }

    public function contact()
    {
        $this->load->view('contact');
    }

    public function blog()
    {
        $blogs = $this->Common_Model->get_all('blog');
        $data = null;
        if($blogs !=""){
            $data['blogs'] = $blogs;
        }
        $this->load->view('blog',$data);
    }

    public function blogDetails()
    {
        $id = segment(2);
        $data['blogDetails'] = $this->Common_Model->get_where('blog',array('id'=>$id));
        $data['comment'] = $this->Common_Model->get_where('comments',array('commented_on' => $id));
        if($data['blogDetails']!="")
        $this->load->view('blog-details',$data);
    }

    public function services()
    {
        $this->load->view('services');
    }

    public function sitemap()
    {
        $this->load->view('sitemap');
    }


}
