<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PriceEstimator extends CI_Controller
{
    public $Common_Model;
        public function __construct(){
            parent::__construct();
            $this->Common_Model = new Common_Model();
        }

        public function priceEstimator()
        {
            $data = array(
                'service' => 'lawn_services',
                'property_address' => 'property_address',
                'created_date' => getCurrentDateTime()
            );
            $result = $this->Common_Model->insert('price_estimator',$data);
            if($result){
                $data = array(
                    'service' => $_POST['lawn_services'],
                    'property_address' => $_POST['property_address'],

                );

                $mail = new PHPMailer;

                $mail->SMTPDebug = 0;
                $mail->isSMTP();
                $mail->Host = "smtp.gmail.com";
                $mail->SMTPAuth = true;
                $mail->Username = PHPMAILER_USERNAME;
                $mail->Password = PHPMAILER_PASSWORD;
                $mail->SMTPSecure = "tls";
                $mail->Port = 587;

                $mail->From = NO_REPLY_EMAIL;
                $mail->FromName = SITE_NAME;

                $mail->addAddress(ADMIN_EMAIL, SITE_NAME);

                $contact_emailer = $this->load->view('estimator_email',array('data' => $data),true);
                $mail->isHTML(true);
                $mail->Subject = "Subject Text";
                $mail->Body = $contact_emailer;
                if (!$mail->send()) {
                    $this->session->set_flashdata("Error Sending Message");
                } else {
                    $this->session->set_flashdata('message', "Your query sent successfully. We'll get back to you soon!");
                }

                redirect($_SERVER['HTTP_REFERER']);
            }

            }
        }

