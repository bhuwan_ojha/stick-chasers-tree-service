<?php

/**
 * Created by PhpStorm.
 * User: Nepsrock
 * Date: 2017-03-15
 * Time: 4:41 PM
 */
class Comment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->CommonModel = new Common_Model();
    }

    function AddComment()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'commented_on'=>$this->input->post('artical'),
            'commented_date' => getCurrentDateTime(),
            'comment' => $this->input->post('message')
        );

        $comment = $this->CommonModel->insert('comments',$data);
        if($comment){
            echo "Success!";
        }else{
            echo "Error!";
        }
    }


}