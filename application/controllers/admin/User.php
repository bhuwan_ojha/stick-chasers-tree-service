<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/15/2016
 * Time: 3:49 PM
 */

class User extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(!check_user('current_user')) {
            redirect('admin');
        }
    }

    function index(){
        $this->load->view('admin/user-manager');
    }

} 