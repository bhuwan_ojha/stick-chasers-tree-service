<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/14/2016
 * Time: 12:42 PM
 */


class Login extends CI_Controller {

    public $CommonModel;

    public function __construct(){
        parent::__construct();
        if(check_user('current_user')) {

        }
        $this->CommonModel = new Common_Model();

    }

    public function Index(){
        $data = 'admin/login';
        $this->load->view($data);
    }

    public function Authenticate(){
        $user = $this->input->post();
        $user = $this->CommonModel->get_where('tbl_admin', array('username' => $user['username'], 'password' => SHA1($user['password'])));
        if($user) {
            $this->session->set_userdata(array(
                'current_user' => $user[0]['id'],
                'username' => $user[0]['username']
            ));
            redirect('admin');
        } else {
            set_flash('msg', 'Incorrect username or password');
           redirect('admin');
        }
    }

    public function UpdateUser(){
       $user = $this->input->post();
        $user['username'] = $this->input->post('username');
        $user['email'] = $this->input->post('email');
        $user['password'] = SHA1($this->input->post('password'));
        $user['id'] = $this->input->post('id');
        $result = $this->CommonModel->update('tbl_admin',$user,array('id' => $user['id']));
        if($result==TRUE){
           set_flash('msg','Successfully Updated User');
        }else{
            set_flash('msg','Failed Updating User');
        }
    }

    public function user(){
        $data = $this->CommonModel->get_all($table='tbl_admin');
            $this->load->view('admin/user-manager', array("data" => $data) );
        }

    function logout()
    {
        $this->session->unset_userdata(array('current_user' => '', 'username' => ''));
        session_destroy();
        redirect('admin');
    }




} 