<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route = array(
    'default_controller' => 'Pages',
    'about' => 'Pages/about',
    'pricing' => 'Pages/pricing',
    'gallery' => 'Pages/gallery',
    'maintenance' => 'Pages/maintenance',
    'contact' => 'Pages/contact',
    'blog' => 'Pages/blog',
    'blog-details/(:num)' => 'Pages/blogDetails/$1',
    'home' => 'Pages/index',
    'services' => 'Pages/services',
    'sitemap' => 'Pages/sitemap',
    'price-estimator' => 'PriceEstimator/priceEstimator',
    'comment-submit' => 'Comment/AddComment',
    'comment-count' =>'Comment/CommentCounts',




    'admin' => 'admin/Index',
    'admin/logout' => 'admin/Login/logout',
    'admin/dashboard' => 'admin/Dashboard',
    'admin/Auth' => 'admin/Login/Authenticate',
    'admin/user' => 'admin/Login/user',
    'admin/content' => 'admin/Pages',
    'admin/banner' => 'admin/Image',
    'admin/form' => 'admin/Pages/form',
    'admin/add-pages' => 'admin/Pages/SaveUpdate',
    'admin/add-image' => 'admin/Image/addImage',
    'admin/form-edit/(:num)' => 'admin/Pages/formEdit/$1',
    'admin/seo-edit/(:num)' => 'admin/Seo/add_update/$1',
    'admin/meta-tag' => 'admin/Seo/common',
    'admin/seo-manager' => 'admin/Seo/index',
    'admin/seo-form' => 'admin/Seo/form',
    'admin/save-update' => 'admin/Seo/add_update',
    'admin/testimonial' => 'admin/Testimonial',




    '404_override' => '',
    'translate_uri_dashes' => FALSE
);
