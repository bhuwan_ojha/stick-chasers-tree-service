<?php include "includes/header.php";?>
<div class="wrapper">
		<?php include "includes/nav-bar.php";?>
	
		<div id="pageContent" class="page-content">
			<section class="content" id="slider">
				<div class="tp-banner-container">
					<div class="tp-banner">
						<ul>
							<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
								<img src="<?php BASE_URL();?>assets/images/slides/slide-1.jpg" alt="slide1" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
								<div class="tp-caption tp-caption--wd-1  lfb ltt" data-x="540" data-y="255" data-speed="600" data-start="800" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 1;">We Work Hard</div>
								<div class="tp-caption tp-caption--wd-2 lfb ltt" data-x="570" data-y="320" data-speed="600" data-start="900" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 2;">on Your Yard</div>
								<div class="tp-caption tp-caption--wd-3 lfb ltt" data-x="600" data-y="410" data-speed="630" data-start="1000" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 3;">Work of exceptional quality since 1995</div>
							</li>
							<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
								<img src="<?php BASE_URL();?>assets/images/slides/slide-4.jpg" alt="slide4" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">

								<div class="tp-caption tp-caption--wd-1  lfb ltt" data-x="center" data-y="center" data-voffset="-100" data-hoffset="0" data-speed="600" data-start="800" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 1;">LAWN SERVICE</div>
								<div class="tp-caption tp-caption--wd-3 lfb ltt" data-x="center" data-y="center" data-voffset="-50" data-hoffset="0" data-speed="600" data-start="900" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 2;">Our revive, sustain & thrive services ensure the health of your lawn</div>
								<a href="#" class="tp-caption lfb ltt linkbtn" data-x="center" data-y="center" data-voffset="30" data-hoffset="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 3;">GET A QUOTE</a> </li>
							<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
								<img src="<?php BASE_URL();?>assets/images/slides/slide-2.jpg" alt="slide2" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
								<div class="tp-caption tp-caption--wd-1  lfb ltt" data-x="center" data-y="center" data-voffset="-100" data-hoffset="0" data-speed="600" data-start="800" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 1;">landscape design</div>
								<div class="tp-caption tp-caption--wd-3 lfb ltt" data-x="center" data-y="center" data-voffset="-50" data-hoffset="0" data-speed="600" data-start="900" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 2;">custom landscape maintenance to complement and enhance your home</div>
								<a href="#" class="tp-caption lfb ltt linkbtn" data-x="center" data-y="center" data-voffset="30" data-hoffset="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 3;">GET A QUOTE</a> </li>
							<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
								<img src="<?php BASE_URL();?>assets/images/slides/slide-3.jpg" alt="slide3" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
								<div class="tp-caption tp-caption--wd-1  lfb ltt" data-x="center" data-y="center" data-voffset="-100" data-hoffset="0" data-speed="600" data-start="800" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 1;">SNOW REMOVAL</div>
								<div class="tp-caption tp-caption--wd-3 lfb ltt" data-x="center" data-y="center" data-voffset="-50" data-hoffset="0" data-speed="600" data-start="900" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 2;">Avoid condo fees, let us take care of your home snow removal for less</div>
								<a href="#" class="tp-caption lfb ltt linkbtn" data-x="center" data-y="center" data-voffset="30" data-hoffset="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 3;">GET A QUOTE</a> </li>
						</ul>
					</div>
				</div>
			</section>

			<section class="content content--fill content--fill--light top-null">
				<div class="container">
					<div class="services-block">
						<div class="service iconmove dark">
							<div class="service-image"><img src="<?php BASE_URL();?>assets/images/service-1-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-icon"><i class="icon first icon-gardener"></i></div>
										<div class="service-title">Lawn Service</div>
										<div class="service-text">We offer total lawn care and routine maintenance to keep your lawn looking brand new</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove light ">
							<div class="service-image image-scale"><img src="<?php BASE_URL();?>assets/images/service-2-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-title">Landscape
											<br> Design Services </div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove light">
							<div class="service-image"><img src="<?php BASE_URL();?>assets/images/service-3-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-icon"><i class="icon first icon-tree"></i></div>
										<div class="service-title">Leaf Removal</div>
										<div class="service-text">Our crew blows leaves to an area of your choice ie: woods, garden, or compost pile</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove light">
							<div class="service-image image-scale"><img src="<?php BASE_URL();?>assets/images/service-4-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-title">Snow Removal</div>
										<div class="service-text">We don’t miss a beat when it comes to digging your home or office out from under the snow.</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove color">
							<div class="service-image"><img src="<?php BASE_URL();?>assets/images/service-5-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-icon"><i class="icon first icon-truck-transport"></i></div>
										<div class="service-title">Junk Removal</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove light">
							<div class="service-image image-scale"><img src="<?php BASE_URL();?>assets/images/service-6-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-title">New Tree Planting</div>
										<div class="service-text">Let us help you protect your investment and let your new trees thrive!</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<h2 class="text-center lined">About Us</h2>
					<p class="text-center info-text">Starting out with just a single truck and mower, we have expanded our services and grown into one of the largest lawn maintenance companies in our area. Our expansion and stellar reputation is due, in part, to our exceptional reputation for quality and timely service. Our lawn care technicians utilize the latest technology and techniques to deliver beautiful results that will stand the test of time.</p>
					<div class="row">
						<div class="col-sm-6 animation" data-animation="fadeInLeft" data-animation-delay="0.5s">
							<div class="text-icon__title">Our Mission</div>
							<div class="text-icon last">
								<div class="text-icon__icon"><i class="icon icon-nature-2"></i></div>
								<div class="text-icon__info">
									<p>Our mission is to provide our customers with the highest level of quality services. We pledge to establish lasting relationships with our clients by exceeding their expectations and gaining their trust through exceptional performance. </p>
								</div>
							</div>
						</div>
						<div class="col-sm-6 animation" data-animation="fadeInRight" data-animation-delay="0.5s">
							<div class="text-icon__title">Our Clients</div>
							<div class="text-icon last">
								<div class="text-icon__icon"><i class="icon icon-construction-worker"></i></div>
								<div class="text-icon__info">
									<p>Our clients count on our dependability, our drive, and our integrity and we take great pride in our accomplishments and build on them every day.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="content content--parallax top-null" data-image="<?php BASE_URL();?>assets/images/parallax-bg.jpg">
				<div class="container">
					<h2 class="text-center white lined">Our Difference</h2>
					<div class="iconcircle-wrap">
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-transport"></i></span> </div>
							<div class="iconcircle__title"> Clean, Branded Vehicles </div>
						</div>
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-people"></i></span> </div>
							<div class="iconcircle__title"> Professional, Uniformed Personnel </div>
						</div>
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-phone-auricular"></i></span> </div>
							<div class="iconcircle__title"> Timely Response Guarantee </div>
						</div>
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-lawn-mowet"></i></span> </div>
							<div class="iconcircle__title"> Safe, Reliable Equipment Maintained Daily </div>
						</div>
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-analytic-report"></i></span> </div>
							<div class="iconcircle__title"> Status and Quality Reports Delivered Timely </div>
						</div>
					</div>
				</div>
			</section>
			<section class="content content--fill content--fill--light top-null">
				<div class="container">
					<div class="row">
						<div class="col-md-8 animation" data-animation="fadeIn" data-animation-delay="0.6s">
							<h2 class="text-center lined">Video Presentation </h2>
							<div class="video-responsive">
								<iframe src="http://www.youtube.com/embed/-PWxUQCNGtg?wmode=opaque" allowfullscreen></iframe>
							</div>
						</div>
						<div class="divider divider--md visible-sm visible-xs"></div>
						<div class="col-md-4 animation" data-animation="fadeIn" data-animation-delay="0.9s">
							<h2 class="text-center lined">Testimonials</h2>
							<div class="testimonials">
								<div class="testimonials-carousel">
									<div class="testimonials__item white">
										<div class="testimonials__item__text"> We used to be the eye sore of the block, after 1 year of service with Lawn Care we now are receiving compliments on our yard! Couldn’t be happier!
											<div class="testimonials__item__username">Ryan, Manson MN</div>
										</div>
										<div class="testimonials__item__meta">
											<div class="testimonials__item__userpic"><img src="<?php BASE_URL();?>assets/images/testimonials-author-img-01.jpg" alt="" /></div>
											<div class="testimonials__item__rating"><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i></div>
										</div>
									</div>
									<div class="testimonials__item white">
										<div class="testimonials__item__text"> Thank you very much for a great job. Our lawn is finally looking so much better. You are the best.
											<div class="testimonials__item__username">Todd, Hudson WI</div>
										</div>
										<div class="testimonials__item__meta">
											<div class="testimonials__item__userpic"><img src="<?php BASE_URL();?>assets/images/testimonials-author-img-02.jpg" alt="" /></div>
											<div class="testimonials__item__rating"><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i><i class="icon icon-star"></i></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="content">
				<div class="container">
					<h2 class="text-center lined">Our Gallery</h2>
					<div class="container">
						<section class="filters-by-category">
							<div class="container">
								<ul class="option-set" data-option-key="filter">
									<li><a href="#filter" data-option-value="*" class="selected">All</a></li>
									<li><a href="#filter" data-option-value=".category1" class="">Lawn Service</a></li>
									<li><a href="#filter" data-option-value=".category2" class="">Landscape Design</a></li>
									<li><a href="#filter" data-option-value=".category3" class="">Leaf Removal</a></li>
									<li><a href="#filter" data-option-value=".category4" class="">Snow Removal</a></li>
									<li><a href="#filter" data-option-value=".category5" class="">Junk Removal</a></li>
									<li><a href="#filter" data-option-value=".category6" class="">Planting New Tree</a></li>
								</ul>
							</div>
						</section>
						<div class="gallery gallery-isotope" id="gallery">
							<div class="gallery__item doubleW doubleH category5 category3">
								<div class="gallery__item__image"> <img src="<?php BASE_URL();?>assets/images/gallery/gallery-img-14.jpg" alt="" /> <a class="btn btn--round" href="<?php BASE_URL();?>assets/images/gallery/gallery-img-14.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
							</div>
							<div class="gallery__item category6 category2">
								<div class="gallery__item__image"> <img src="<?php BASE_URL();?>assets/images/gallery/gallery-img-15.jpg" alt="" /> <a class="btn btn--round" href="<?php BASE_URL();?>assets/images/gallery/gallery-img-15.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
							</div>
							<div class="gallery__item category6 category1">
								<div class="gallery__item__image"> <img src="<?php BASE_URL();?>assets/images/gallery/gallery-img-16.jpg" alt="" /> <a class="btn btn--round" href="<?php BASE_URL();?>assets/images/gallery/gallery-img-16.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
							</div>
							<div class="gallery__item category4">
								<div class="gallery__item__image"> <img src="<?php BASE_URL();?>assets/images/gallery/gallery-img-17.jpg" alt="" /> <a class="btn btn--round" href="<?php BASE_URL();?>assets/images/gallery/gallery-img-17.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
							</div>
							<div class="gallery__item category6 category2">
								<div class="gallery__item__image"> <img src="<?php BASE_URL();?>assets/images/gallery/gallery-img-18.jpg" alt="" /> <a class="btn btn--round" href="<?php BASE_URL();?>assets/images/gallery/gallery-img-18.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
							</div>
							<div class="gallery__item category1">
								<div class="gallery__item__image"> <img src="<?php BASE_URL();?>assets/images/gallery/gallery-img-03.jpg" alt="" /> <a class="btn btn--round" href="<?php BASE_URL();?>assets/images/gallery/gallery-img-03.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
							</div>
							<div class="gallery__item category2">
								<div class="gallery__item__image"> <img src="<?php BASE_URL();?>assets/images/gallery/gallery-img-04.jpg" alt="" /> <a class="btn btn--round" href="<?php BASE_URL();?>assets/images/gallery/gallery-img-04.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
							</div>
						</div>
						<div class="divider divider--md"></div>
						<div class="text-center"><a class="btn btn--wd">MORE</a></div>
						<div class="divider divider--md"></div>
					</div>
				</div>
			</section>
		</div>
		<?php include "includes/footer.php";?>
</body>
</html>