<footer class="footer" id="free-estimation">
			<div class="footer__top">
				<div class="container" >
					<form id="estimate-form" class="estimate-form" action="<?php echo BASE_URL();?>price-estimator" method="post">
						<div class="row" >
							<div class="col-md-3">
                                <div class="title" data-toggle="collapse" data-target="#free-estimator-collapase"><span class="collabsable">Free Estimate</span></div>
							</div>
                            <div  id="free-estimator-collapase">
							<div class="col-md-3">
								<div>I want a Free estimate for </div>
								<div class="select-wrapper select--full">
									<select class="select--wd" id="lawn-services" name="lawn_services">
										<option value="Hazardous Tree Removal">Hazardous Tree Removal</option>
										<option value="Tree Prunings">Tree Prunings</option>
										<option value="Weight Reduction">Weight Reduction</option>
										<option value="Dead Wooding">Dead Wooding</option>
										<option value="Selective Thining">Selective Thining</option>
										<option value="Shape and Appearance">Shape and Appearance</option>
										<option value="Hard Prunings of Shrubs & Ornamentals">Hard Prunings of Shrubs & Ornamentals</option>
										<option value="Lot Clearing / Bush Removal">Lot Clearing / Bush Removal</option>
										<option value="Stump Grinding">Stump Grinding</option>
										<option value="Seasoned Split Firewood Sales">Seasoned Split Firewood Sales</option>
										<option value="Snow Plowing">Snow Plowing</option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div>at</div>
								<input type="text" class="subscribe-form__input input--wd input--full" name="property_address" id="property-address" placeholder="Property address" > </div>
							<div class="col-md-2">
								<div>&nbsp;</div>
								<button type="button" class="g-recaptcha btn btn--wd" data-callback="onSubmit" data-sitekey="<?php echo RECAPTCHA_KEY; ?>" style="margin-top: 1px;">Submit</button>
							</div>
						</div>
                        </div>
					</form>
				</div>
			</div>
			<div class="footer__column-links">

				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-3"> <img src="<?php echo BASE_URL();?>assets/images/guarantee.png" class="img-responsive" alt="" /> </div>
						<div class="col-sm-6 col-md-3 mobile-collapse">
							<h5 class="title mobile-collapse__title">Information </h5>
							<div class="v-links-list mobile-collapse__content">
								<ul>
									<li><a href="#" id="contact-us">Contact Us</a></li>
									<!--<li><a href="<?php /*echo BASE_URL();*/?>about">Testimonials</a></li>-->
									<li><a href="<?php echo BASE_URL();?>about">Careers</a></li>
									<li><a href="<?php echo BASE_URL();?>sitemap">Sitemap</a></li>
									<!--<li><a href="<?php /*echo BASE_URL();*/?>#">Terms of Use</a></li>-->
								</ul>
							</div>
						</div>
						<div class="col-sm-6 col-md-3 mobile-collapse">
							<h5 class="title mobile-collapse__title">Services</h5>
							<div class="v-links-list mobile-collapse__content">
								<ul>
									<li><a href="<?php echo BASE_URL();?>#">Service 1</a></li>
									<li><a href="<?php echo BASE_URL();?>#">Service 2</a></li>
									<li><a href="<?php echo BASE_URL();?>#">Service 3</a></li>
									<li><a href="<?php echo BASE_URL();?>#">Service 4</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-6 col-md-3 mobile-collapse mobile-collapse--last">
							<h5 class="title mobile-collapse__title">Company Info</h5>
							<div class="v-links-list mobile-collapse__content">
								<ul>
									<li><?php echo SITE_ADDRESS;?></li>
									<li><a href="mailto:<?php echo SITE_EMAIL;?>"><?php echo SITE_EMAIL;?></a></li>
									<li>Office - <span class="phone-number"><?php echo SITE_NUMBER;?> </span> <i class="icon-telephone"></i></li>
                                    <li>Dave's Cell - <span class="phone-number">262-880-5774 </span> <i class="icon-telephone"></i></li>
                                    <li>Jennifer's Cell - <span class="phone-number">920-296-2820 </span> <i class="icon-telephone"></i></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer__bottom">
				<div class="container">
					<div class="pull-right col-sm-6 col-md-3 col-lg-3">
						<div class="social-links">
							<ul>
								<!--<li>
									<a class="icon icon-twitter-logo-button" href="#"></a>
								</li>-->
								<li>
									<a class="icon icon-facebook-logo-button" href="https://www.facebook.com/Stick-Chasers-Tree-Service-728901497247114/" target="_blank"></a>
								</li>

								<!--<li>
									<a class="icon icon-instagram-logo" href="#"></a>
								</li>-->
							</ul>
						</div>
					</div>

					<div class="pull-right col-sm-6 col-md-5 col-lg-4">
						<div class="phone">
							<div><span class="custom-color hidden-xs">CALL US: </span> <span class="phone-number"><?php echo SITE_NUMBER;?></span><i class="icon icon-telephone"></i></div>
							<div class="phone-sm">Emergency Hours - 24 hours a day 7 days a week </div>
							<div class="phone-sm">Normal hours - 8 to 4 Monday To Friday </div>
						</div>
					</div>


					<div class="pull-left col-sm-12 col-md-3 col-lg-4">
						<div class="copyright">© 2016 - <?php echo DATE_TIME;?> <a href="#"><?php echo SITE_NAME;?></a></div>
                        <div><strong>Fully Insured / 20+ Years of Experience</strong></div>

					</div>
                    <div class="col-sm-12">
                        <div class="back-to-top pull-right" style="margin-top: 10px;"><a href="#top"><span class="icon-tree"></span></a></div>
                    </div>
				</div>
			</div>
		</footer>
	</div>

	<script src="<?php echo BASE_URL();?>assets/vendor/jquery/jquery.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/bootstrap/bootstrap.min.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/waves/waves.min.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/slick/slick.min.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/parallax/jquery.parallax-1.1.3.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/waypoints/sticky.min.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/doubletaptogo/doubletaptogo.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/isotope/isotope.pkgd.min.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/form/jquery.form.js"></script>
	<script src="<?php echo BASE_URL();?>assets/vendor/form/jquery.validate.min.js"></script>
	<script src="<?php echo BASE_URL();?>assets/js/custom.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL();?>assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="<?php echo BASE_URL();?>assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript">
		jQuery(document).ready(function () {
			var fullwidth = "on";
			var fullscreen = "off";
			jQuery('.tp-banner').show().revolution({
				dottedOverlay: "none"
				, delay: 5000
				, startwidth: 1920
				, startheight: 746
				, hideThumbs: 200
				, hideTimerBar: "on"
				, thumbWidth: 100
				, thumbHeight: 50
				, thumbAmount: 5
				, navigationType: "none"
				, navigationArrows: ""
				, navigationStyle: ""
				, touchenabled: "on"
				, onHoverStop: "off"
				, swipe_velocity: 0.7
				, swipe_min_touches: 1
				, swipe_max_touches: 1
				, drag_block_vertical: false
				, parallax: "mouse"
				, parallaxBgFreeze: "on"
				, parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0]
				, keyboardNavigation: "off"
				, navigationHAlign: "center"
				, navigationVAlign: "bottom"
				, navigationHOffset: 0
				, navigationVOffset: 20
				, soloArrowLeftHalign: "left"
				, soloArrowLeftValign: "center"
				, soloArrowLeftHOffset: 20
				, soloArrowLeftVOffset: 0
				, soloArrowRightHalign: "right"
				, soloArrowRightValign: "center"
				, soloArrowRightHOffset: 20
				, soloArrowRightVOffset: 0
				, shadow: 0
				, fullWidth: fullwidth
				, fullScreen: fullscreen
				, spinner: ""
				, stopLoop: "off"
				, stopAfterLoops: -1
				, stopAtSlide: -1
				, shuffle: "off"
				, autoHeight: "off"
				, forceFullWidth: "off"
				, hideThumbsOnMobile: "off"
				, hideNavDelayOnMobile: 1500
				, hideBulletsOnMobile: "off"
				, hideArrowsOnMobile: "off"
				, hideThumbsUnderResolution: 0
				, hideSliderAtLimit: 0
				, hideCaptionAtLimit: 0
				, hideAllCaptionAtLilmit: 0
				, startWithSlide: 0
				, fullScreenOffsetContainer: ".header .navbar-wd"
			});
		});
	</script>
<script>
    function onSubmit(){
        var lawnServices = $('#lawn-services').val();
        var propertyAddress = $('#property-address').val();
        if(lawnServices && propertyAddress !=''){
            document.getElementById("estimate-form").submit();
        }
    }
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>
    $('.quotes').slick({
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 6000,
        speed: 800,
        slidesToShow: 1,
        adaptiveHeight: true
    });
    $( document ).ready(function() {
        $('.no-fouc').removeClass('no-fouc');
    });
</script>