<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<title>Stick Chasers Tree Service</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link href="<?php echo BASE_URL();?>apple-touch-icon.png" rel="apple-touch-icon">

	<link href="<?php echo BASE_URL();?>apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76">

	<link href="<?php echo BASE_URL();?>apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120">

	<link href="<?php echo BASE_URL();?>apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152">

	<link href="<?php echo BASE_URL();?>apple-touch-icon-152x152.png" rel="apple-touch-icon-precomposed">

	<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/font/style.css">

	<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/vendor/waves/waves.css">

	<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/vendor/slick/slick.css">

	<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/vendor/slick/slick-theme.css">

	<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/vendor/magnific-popup/magnific-popup.css">

	<!--<link rel="stylesheet" href="<?php /*echo BASE_URL();*/?>assets/css/style.css">-->

    <?php include "assets/css/style.php";?>

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700">

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">

	<link rel="stylesheet" href="<?php echo BASE_URL();?>assets/vendor/rs-plugin/css/settings.css" media="screen">

	<script src="<?php echo BASE_URL();?>assets/vendor/modernizr/modernizr.js"></script>

    <script src='https://www.google.com/recaptcha/api.js'></script>



</head>