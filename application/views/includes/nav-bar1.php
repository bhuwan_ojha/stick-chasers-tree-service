<body>
<div id="loader-wrapper" class="loader-on">
    <div id="loader">
        <div class="spinner"> <span class="ball-1"></span> <span class="ball-2"></span> <span class="ball-3"></span> <span class="ball-4"></span> </div>
    </div>
</div>
<div class="overlay overlay-scale">
    <button type="button" class="overlay-close"> ✕ </button>
    <div class="overlay__content">
        <form id="search-form" class="search-form outer" action="#" method="post">
            <div class="input-group input-group--wd">
                <input type="text" class=" input--full" placeholder="search text here ..."> </div>
            <button class="btn btn--wd text-uppercase wave waves-effect">Search</button>
        </form>
    </div>
</div>
<header class="header header--max header--sticky">
    <nav class="navbar navbar-wd" id="navbar">
        <div class="navbar-header">
            <div class="container">
                <button type="button" class="navbar-toggle" id="slide-nav"><i class="icon icon-menu"></i></button>
                <div class="row">
                    <div class="col-sm-3 col-lg-3">
                        <a class="logo" href="<?php echo BASE_URL();?>"> <img class="logo-default" src="<?php echo BASE_URL();?>assets/images/logo.png" alt="" /> <img class="logo-mobile" src="<?php echo BASE_URL();?>assets/images/logo.png" alt="" /> </a>
                    </div>
                    <div class="visible-lg col-lg-3">
                        <div class="slogan" style="margin-bottom: -60px;"> Stick Chasers Tree Service </div>
                        <div class="slogan"> Quality work you Can Trust! </div>

                    </div>
                    <div class="col-sm-6 col-lg-4">
                        <div class="phone">
                            <div><span class="custom-color hidden-xs">CALL US: </span> <span class="phone-number"><?php echo SITE_NUMBER;?></span><i class="icon icon-telephone"></i></div>
                            <div class="phone-sm hidden-xs">Emergency Hours - 24 hours a day 7 days a week </div>
                            <div class="phone-sm hidden-xs">Normal hours - 8 to 4 Monday To Friday </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-lg-2">
                        <div class="social-links">
                            <ul>
                                <!--<li>
                                    <a class="icon icon-twitter-logo-button" href="#"></a>
                                </li>-->
                                <li>
                                    <a class="icon icon-facebook-logo-button" href=" https://www.facebook.com/Stick-Chasers-Tree-Service-728901497247114/" target="_blank"></a>
                                </li>
                                <!--<li>
                                    <a class="icon icon-instagram-logo" href="#"></a>
                                </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header__search"> <a href="#" class="search-open"><span class="icon icon-magnifying-glass"></span></a> </div>
            </div>
        </div>
        <div id="slidemenu">
            <div class="slidemenu-close visible-xs">✕</div>
            <div class="container">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo BASE_URL();?>home"><span class="link-name">Home</span></a></li>
                    <li><a href="<?php echo BASE_URL();?>home#about-us-section" id="about-us"><span class="link-name">About Us</span></a></li>
                    <li><a href="<?php echo BASE_URL();?>home#services-section" id="services" class="dropdown-toggle"><span class="link-name">Services</span><!--<span class="caret icon-angle-down"></span>--></a>
                        <!--<ul class="dropdown-menu animated fadeIn" role="menu">
                            <li class="level2"><a href="#">Hazardous Tree Removal</a></li>
                            <li class="level2"><a href="#">Tree Prunings</a></li>
                            <li class="level2"><a href="#">Weight Reduction</a></li>
                            <li class="level2"><a href="#">Dead Wooding</a></li>
                            <li class="level2"><a href="#">Selective Thining</a></li>
                            <li class="level2"><a href="#">Shape and Appearance</a></li>
                            <li class="level2"><a href="#">Hard Prunings of Shrubs & Ornamentals</a></li>
                            <li class="level2"><a href="#">Lot Clearing / Bush Removal</a></li>
                            <li class="level2"><a href="#">Stump Grinding</a></li>
                            <li class="level2"><a href="#">Seasoned Split Firewood Sales</a></li>
                            <li class="level2"><a href="#">Snow Plowing</a></li>
                        </ul>-->
                    </li>
                    <!--<li><a href="#" id="pricing"><span class="link-name">Pricing</span></a></li>-->
                    </li>
                    <li><a href="<?php echo BASE_URL();?>home#gallery-section" id="gallery"><span class="link-name">Gallery</span></a></li>
                    <!--<li><a href="#" id="maintenance"><span class="link-name">Maintenance tips</span></a></li>-->
                    <li><a href="<?php echo BASE_URL();?>home#contact-section" id="contact"><span class="link-name">Contact Us</span></a></li>
                    <li><a href="<?php echo BASE_URL();?>blog"><span class="link-name">Blog</span></a></li>

                </ul>
            </div>
        </div>
    </nav>
</header>