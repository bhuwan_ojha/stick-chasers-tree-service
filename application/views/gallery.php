<?php include "includes/header.php";?>
<div class="wrapper">
    <?php include "includes/nav-bar.php";?>
    <div id="pageContent" class="page-content">
        <section class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
                    <li><a href="<?php echo BASE_URL();?>home">Home</a></li>
                    <li class="active">Gallery</li>
                </ol>
            </div>
        </section>
        <section class="content content--fill content--fill--light fullwidth top-null">
            <div class="container">
                <h1 class="text-center lined">Gallery</h1>
                <section class="filters-by-category">
                    <div class="container">
                        <ul class="option-set" data-option-key="filter">
                            <li><a href="#filter" data-option-value="*" class="selected">All</a></li>
                            <li><a href="#filter" data-option-value=".category1" class="">Lawn Service</a></li>
                            <li><a href="#filter" data-option-value=".category2" class="">Landscape Design</a></li>
                            <li><a href="#filter" data-option-value=".category3" class="">Leaf Removal</a></li>
                            <li><a href="#filter" data-option-value=".category4" class="">Snow Removal</a></li>
                            <li><a href="#filter" data-option-value=".category5" class="">Junk Removal</a></li>
                            <li><a href="#filter" data-option-value=".category6" class="">Planting New Tree</a></li>
                        </ul>
                    </div>
                </section>
                <div class="gallery gallery-isotope" id="gallery">
                    <div class="gallery__item doubleW doubleH category1">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-01.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-01.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category2">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-02.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-02.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category1">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-03.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-03.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category2">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-04.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-04.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category3 category5">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-05.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-05.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category4">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-06.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-06.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category2">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-07.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-07.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category2">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-08.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-08.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category3">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-09.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-09.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category6">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-10.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-10.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item doubleW doubleH category3">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-14.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-14.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category4">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-11.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-11.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category1">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-12.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-12.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                    <div class="gallery__item category2">
                        <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-13.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-13.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                    </div>
                </div>
                <div class="divider divider--md"></div>
                <div class="text-center"><a class="btn btn--wd view-more-gallery" data-load="gallery-more.html">More photos</a></div>
                <div id="galleryPreload"></div>
            </div>
        </section>
        <div class="page-bot"><img src="<?php echo BASE_URL();?>assets/images/under-footer.png" class="img-responsive" alt=""></div>
    </div>
    <?php include "includes/footer.php";?>
    </body>
    </html>