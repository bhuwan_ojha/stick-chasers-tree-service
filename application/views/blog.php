<?php include "includes/header.php";?>
<div class="wrapper">
    <?php include "includes/nav-bar1.php";?>
    <div id="pageContent" class="page-content">
        <section class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
                    <li><a href="<?php echo BASE_URL();?>home">Home</a></li>
                    <li class="active">Blog</li>
                </ol>
            </div>
        </section>
        <section class="content content--fill content--fill--light top-null">
            <div class="container">
                <h1 class="text-center lined">Blog Posts</h1>
                <div class="row">
                    <div class="col-md-8 column-center">
                        <div class="blog-post">
                            <div class="post-image">
                                <a href="#"><img src="<?php echo BASE_URL();?>assets/images/blog/blog-post-img-1.jpg" alt=""></a>
                            </div>
                            <?php foreach ($blogs as $blog):?>
                            <div class="post-content">
                                <h3 class="post-title"><a href="<?php echo BASE_URL();?>blog-details/<?php echo $blog['id'];?>"><?php echo $blog['blog_title'];?></a></h3>
                                <div class="post-date"><span class="day" style="font-size: 25px!important;"><?php echo date("jS", strtotime($blog['created_date']));?></span><span class="month"><?php echo date("F", strtotime($blog['created_date']));?></span></div>
                                <div class="post-author"><?php echo $blog['created_by'];?></div>
                                <div class="post-teaser">
                                    <p><?php echo $blog['blog_description'];?>...</p>
                                </div>
                                <?php $comment_count = $this->Common_Model->get_count('comments',array('commented_on' => $blog['id'])); ?>
                                <div class="post-meta"><i class="icon icon-speech"></i><span id="comments"><?php echo $comment_count;?></span></div>
                            </div>
                            <?php endforeach; ?>
                        </div>

                        <div class="divider"></div>
                        <div class="text-center"><a class="btn btn--wd view-more-post" data-load="post-more-ajax.txt">More Posts</a></div>
                        <div id="postPreload"></div>


                    </div>
                    <div class="col-md-4 column-right">
                        <div class="side-block">
                            <h4>Archives</h4>
                            <div class="calendar">
                                <div class="calendar__header">November 2016</div>
                                <table>
                                    <tr>
                                        <th>S</th>
                                        <th>M</th>
                                        <th>T</th>
                                        <th>W</th>
                                        <th>Th</th>
                                        <th>F</th>
                                        <th>S</th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>5</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>7</td>
                                        <td>8</td>
                                        <td>9</td>
                                        <td>10</td>
                                        <td>11</td>
                                        <td>12</td>
                                    </tr>
                                    <tr>
                                        <td>13</td>
                                        <td>14</td>
                                        <td>15</td>
                                        <td>16</td>
                                        <td class="selected">17</td>
                                        <td>18</td>
                                        <td>19</td>
                                    </tr>
                                    <tr>
                                        <td>20</td>
                                        <td>21</td>
                                        <td>22</td>
                                        <td>23</td>
                                        <td>24</td>
                                        <td>25</td>
                                        <td>26</td>
                                    </tr>
                                    <tr>
                                        <td>27</td>
                                        <td>28</td>
                                        <td>29</td>
                                        <td>30</td>
                                        <td>31</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <div class="calendar__footer"><a href="#">« Sep</a></div>
                            </div>
                        </div>
                        <div class="side-block">
                            <h4>Post Categories</h4>
                            <ul class="marker-list">
                                <li><a href="#">Audios  <span>(2)</span></a></li>
                                <li><a href="#">Daily Inspiration  <span>(7)</span></a></li>
                                <li><a href="#">Freelance  <span>(3)</span></a></li>
                                <li><a href="#">Links  <span>(1)</span></a></li>
                                <li><a href="#">Mobile  <span>(1)</span></a></li>
                                <li><a href="#">Photography  <span>(5)</span></a></li>
                                <li><a href="#">Quotes  <span>(1)</span></a></li>
                                <li><a href="#">Resources  <span>(3)</span></a></li>
                                <li><a href="#">Status  <span>(1)</span></a></li>
                            </ul>
                        </div>
                        <div class="side-block">
                            <h4>Popular tags</h4>
                            <ul class="tags-list">
                                <li><a href="#">lawn care</a></li>
                                <li><a href="#">lawn services</a></li>
                                <li><a href="#">decoration</a></li>
                                <li><a href="#">grass</a></li>
                                <li><a href="#">green</a></li>
                                <li><a href="#">flowers</a></li>
                                <li><a href="#">garden tips</a></li>
                                <li><a href="#">bermuda grass</a></li>
                                <li><a href="#">maintaince</a></li>
                            </ul>
                        </div>
                        <div class="side-block">
                            <h4>Popular Posts</h4>
                            <div class="blog-post post-preview">
                                <div class="wrapper">
                                    <div class="post-image">
                                        <a href="<?php echo BASE_URL();?>blog-details"><img src="<?php echo BASE_URL();?>assets/images/blog/blog-post-small-1.jpg" alt=""></a>
                                    </div>
                                    <div class="post-content">
                                        <div class="post-date-sm">10 / 4 / 2016</div>
                                        <h5 class="post-title"><a href="<?php echo BASE_URL();?>blog-details">How to Get the Best Lawn Care Services</a></h5>
                                        <div class="post-author-sm">Ricky B. Brown</div>
                                    </div>
                                </div>
                                <div class="post-meta"><i class="icon icon-speech"></i><span>4</span></div>
                            </div>
                            <div class="blog-post post-preview">
                                <div class="wrapper">
                                    <div class="post-image">
                                        <a href="<?php echo BASE_URL();?>blog-details"><img src="<?php echo BASE_URL();?>assets/images/blog/blog-post-small-2.jpg" alt=""></a>
                                    </div>
                                    <div class="post-content">
                                        <div class="post-date-sm">10 / 4 / 2016</div>
                                        <h5 class="post-title"><a href="<?php echo BASE_URL();?>blog-details">Falling Behind: Lawn Care Services</a></h5>
                                        <div class="post-author-sm">Ricky B. Brown</div>
                                    </div>
                                </div>
                                <div class="post-meta"><i class="icon icon-speech"></i><span>4</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="page-bot"><img src="<?php echo BASE_URL();?>assets/images/under-footer.png" class="img-responsive" alt=""></div>
</div>
<?php include "includes/footer.php";?>
<!--<script>
    $(document).ready(function ()
    {
        var array_id = $("input[name='artical_id\\[\\]']")
            .map(function(){return $(this).val();}).get();

        var id = JSON.stringify(array_id);
        jQuery.each(array_id, function(index, item) {
            // do something with `item` (or `this` is also `item` if you like)
            $.ajax({
                url: '<?php /*echo BASE_URL();*/?>comment-count',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    $('#comments').html(data);
                    debugger;

                }
            });
        });
    });
</script>-->
</body>
</html>