<?php include "includes/header.php";?>
<div class="wrapper">
    <?php include "includes/nav-bar.php";?>
    <div id="pageContent" class="page-content">
        <section class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
                    <li><a href="<?php echo BASE_URL();?>home">Home</a></li>
                    <li class="active">Pricing</li>
                </ol>
            </div>
        </section>
        <section class="content content--fill content--fill--light top-null">
            <div class="container">
                <h1 class="text-center lined">Our Prices</h1>
                <div class="table-responsive">
                    <table class="table price-table">
                        <tbody>
                        <tr>
                            <th colspan="2">
                                <p class="price-info">Choose one of our award-winning programs for automatically-scheduled lawn maintenance and a lush, green lawn.</p>
                            </th>
                            <th><span class="price-title"><i class="icon icon-wheelbarrow"></i>Simple Program</span></th>
                            <th><span class="price-title-color"><i class="icon icon-landkeeper"></i>Extended Program</span></th>
                        </tr>
                        <tr>
                            <td><img src="<?php echo BASE_URL();?>assets/images/pricing-1.jpg" class="img-responsive" alt=""></td>
                            <td>Season-Long Fertilization</td>
                            <td class="text-center"><i class="icon icon-check"></i></td>
                            <td class="text-center"><i class="icon icon-check color"></i></td>
                        </tr>
                        <tr>
                            <td><img src="<?php echo BASE_URL();?>assets/images/pricing-2.jpg" class="img-responsive" alt=""></td>
                            <td>Season-Long Weed Control</td>
                            <td class="text-center"><i class="icon icon-check"></i></td>
                            <td class="text-center"><i class="icon icon-check color"></i></td>
                        </tr>
                        <tr>
                            <td><img src="<?php echo BASE_URL();?>assets/images/pricing-3.jpg" class="img-responsive" alt=""></td>
                            <td>Lawn Analysis With Every Visit</td>
                            <td class="text-center"><i class="icon icon-check"></i></td>
                            <td class="text-center"><i class="icon icon-check color"></i></td>
                        </tr>
                        <tr>
                            <td><img src="<?php echo BASE_URL();?>assets/images/pricing-4.jpg" class="img-responsive" alt=""></td>
                            <td>IPM Surface Insect Control</td>
                            <td class="text-center"></td>
                            <td class="text-center"><i class="icon icon-check color"></i></td>
                        </tr>
                        <tr>
                            <td><img src="<?php echo BASE_URL();?>assets/images/pricing-5.jpg" class="img-responsive" alt=""></td>
                            <td>Mechanical Core Aeration</td>
                            <td class="text-center"></td>
                            <td class="text-center"><i class="icon icon-check color"></i></td>
                        </tr>
                        <tr>
                            <td><img src="<?php echo BASE_URL();?>assets/images/pricing-6.jpg" class="img-responsive" alt=""></td>
                            <td>FREE! Over-Seeding Upgrade</td>
                            <td class="text-center"></td>
                            <td class="text-center"><i class="icon icon-check color"></i></td>
                        </tr>
                        <tr class="actions">
                            <td colspan="2"><a href="#" class="btn btn--wd">Click here for an Instant Online Quote</a></td>
                            <td class="text-center"><a href="#" class="btn btn--wd">Order Now!</a></td>
                            <td class="text-center"><a href="#" class="btn btn--wd">Order Now!</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="divider divider--md"></div>
                <h2 class="text-center lined">Additional Services</h2>
                <p class="info-text text-center"> Thatch problem? Weeds in the interlock? We offer a variety of additional services that you can add to any program.</p>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="price-box">
                            <div class="title">
                                <div class="vert-wrap">
                                    <div class="vert">Weed Control for Paths, Patios and Driveways</div>
                                </div>
                            </div>
                            <div class="text">Starting from just</div>
                            <div class="price">$19.95</div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="price-box">
                            <div class="title">
                                <div class="vert-wrap">
                                    <div class="vert">Dethatching</div>
                                </div>
                            </div>
                            <div class="text">Starting from just</div>
                            <div class="price">$99.95</div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="price-box">
                            <div class="title">
                                <div class="vert-wrap">
                                    <div class="vert">Dethatching +
                                        <br>Overseeding</div>
                                </div>
                            </div>
                            <div class="text">Starting from just</div>
                            <div class="price">$129.95</div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="price-box">
                            <div class="title">
                                <div class="vert-wrap">
                                    <div class="vert">Granular Compost and Seed</div>
                                </div>
                            </div>
                            <div class="text">Starting from just</div>
                            <div class="price">$69.96</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="page-bot"><img src="<?php echo BASE_URL();?>assets/images/under-footer.png" class="img-responsive" alt=""></div>
    </div>
    <?php include "includes/footer.php";?>
    </body>
    </html>