<?php include "includes/header.php";?>
<div class="wrapper">
    <?php include "includes/nav-bar.php";?>
    <div id="pageContent" class="page-content">
        <section class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
                    <li><a href="<?php echo BASE_URL();?>home">Home</a></li>
                    <li class="active">Maintenance Tips</li>
                </ol>
            </div>
        </section>
        <section class="content content--fill content--fill--light top-null">
            <div class="container">
                <h1 class="text-center lined">Maintenance Tips</h1>
                <div class="panel-group">
                    <div class="faq-item">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#faq1">
                                        Lawn Care Equipment Maintenance in the Spring
                                        <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                        <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                    </a>
                                </h4> </div>
                            <div id="faq1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>A little bit of service and preventative maintenance can ensure that your lawnmower, string trimmer and other implements are running efficiently and won't break down before you place them back into storage for the winter. Such machines are typically gasoline powered, so they should be serviced using the following simple steps:
                                    <p>
                                    <p><b>1. Remove the gasoline.</b> Leftover gasoline from the previous year can become stale, choking the carburetor and causing rust.
                                        <br> <b>2. Disconnect the spark plug. </b>This disables the engine, making it safer to perform service on the machine.
                                        <br> <b>3. Remove the blade.</b> This step applies mainly to lawnmowers, though other equipment like edgers also have blades. While this piece is removed, sharpen it using a metal file.
                                        <br> <b>4. Drain the oil. </b>This step doesn't apply to string trimmers and other handheld lawn machines, which typically have two-cycle engines and run on a mixture of gasoline and oil. Four-cycle engines, common on lawnmowers, will need to be drained of oil.
                                        <br> <b>5. Clean the equipment.</b> Use a putty knife and wire brush to knock off accumulated grass and mud, then reattach the blade if you removed one earlier.
                                        <br> <b>6. Fill the oil tank. </b>If you're servicing a four-cycle engine, refill the oil tank with fresh oil.
                                        <br> <b>7. Replace the air filter. </b>This improves airflow to the engine, allowing it to run more smoothly.
                                        <br> <b>8. Replace the spark plug.</b> Although your old spark plug may still work properly, installing a new one is a cheap and easy way to ensure optimal performance.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#faq2" class="collapsed">
                                        How to Mow Your Lawn in the Spring
                                        <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                        <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                    </a>
                                </h4> </div>
                            <div id="faq2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>It may surprise you that there's more to grass cutting than cranking up the lawnmower and pushing it across the lawn. Both mowing height and frequency are important to the health of your grass.</p>
                                    <p>Though it may reduce the number of times you have to mow, cutting your grass short is harmful to your lawn in the long run. Mowing with a low blade height removes nutrients stored in leaf blades and exposes the soil to sunlight, allowing weeds to take hold more easily. Taller grass is better able to compete with weeds, thanks to a larger root system and a higher tolerance for heat. It also shades the ground, allowing the soil to retain water more effectively.</p>
                                    <p>Given these benefits, it's a good idea to cut your grass at the tallest height recommended for your grass type, which are as follows: </p>
                                    <ul class="marker-list">
                                        <li>Common bermudagrass: 1-2 inches (2.5-5 centimeters)</li>
                                        <li>Fescue: 2-3.5 inches (5-9 centimeters)</li>
                                        <li>Kentucky bluegrass: 2-3.5 inches (5-9 centimeters)</li>
                                        <li>St. Augustine: 2-4 inches (5-10 centimeters)</li>
                                        <li>Zoysia: 0.5-1.5 inches (1-4 centimeters)</li>
                                    </ul>
                                    <p>Mow your lawn often enough so that you're only removing the top one-third of the blades. This places less stress on the grass, and the smaller clippings are able to decompose more easily. Avoid bagging these clippings; this added organic matter is actually quite good for the soil.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#faq3" class="collapsed">
                                        Fertilizing Grass in the Spring
                                        <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                        <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                    </a>
                                </h4> </div>
                            <div id="faq3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Once your grass is well-established, you can encourage its growth and discourage weeds by applying a combination of fertilizers and herbicides. When you should apply these substances and how much you should apply depends on several factors, like where you live and the extent of your weed problem.</p>
                                    <p>Fertilizer can help your lawn grow thick and lush, but if it's not used properly, it can actually damage the grass. A slow-release nitrogen fertilizer is best, and no more than 1 pound (0.45 kilograms) of nitrogen should be spread per 1,000 square feet (93 square meters). It should be applied early in the season when the turf begins actively growing, so the timing varies among regions. Fertilizer should not be applied too early or late, however, as lingering cold or early heat can stress the grass. Check the packaging to see when and how much you should water after applying the fertilizer.</p>
                                    <p>Herbicides must also be used with care, as their effectiveness often depends on when they're used. If you have a widespread weed infestation, it's best to apply a pre-emergent herbicide to your lawn before the seeds germinate in the spring. Be aware, however, that you can't use this treatment if you plan to plant new grass, as the herbicide will also prevent those seeds from germinating. For more isolated problems, spot treating with a non-selective herbicide should be enough to do the trick. Ultimately, the best way to discourage weeds is to have a thick, healthy lawn.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#faq4" class="collapsed">
                                        Planting Grass in the Spring
                                        <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                        <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                    </a>
                                </h4> </div>
                            <div id="faq4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Once you've cleaned and repaired your lawn, you may need to reseed parts of it that are particularly bare or brown. This can dramatically improve the appearance of your grass, but there are a few simple steps you should follow to ensure that it won't look worse after you plant than it did before.</p>
                                    <p>First try to address the soil conditions that prevented grass from growing in the past. Call your local Cooperative Extension office to find out where you can get a soil test; this will tell you what nutrients your lawn is lacking. Once you've corrected your soil composition, aerate the ground to avoid any problems with soil compaction.</p>
                                    <p>Now you're ready to buy seed and spread it on your lawn. Before choosing a seed, determine which varieties will work best in your region of the country and with the amount of sunlight in your yard. Then roughly estimate the size of the area where you plan to plant, as seed coverage is recommended in pounds per square foot. If you're spreading the seed over a large area, it is best to use a broadcast spreader, but smaller areas can be seeded by hand.</p>
                                    <p>Don't ignore the grass once you've planted it. Water regularly to maintain soil moisture and fertilize with a slow-release, low-nitrogen product. Mow when the grass reaches 3 or 4 inches (7.6 to 10 centimeters) in height, but try not to trim off more than a half-inch (1-centimeter) as doing so could stress the plant.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#faq5" class="collapsed">
                                        Cleaning and Repairing Your Lawn in Spring
                                        <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                        <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                    </a>
                                </h4> </div>
                            <div id="faq5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If your lawn is already well-maintained, all you need to do is give it a light raking once the ground has dried out. However, problem areas should be addressed quickly, as they can stress your lawn and make it more susceptible to weeds and disease.</p>
                                    <p>One common problem is uneven ground. Low spots cause poor drainage, while high spots are often scalped by the lawn mower. Since these situations create poor growing conditions for grass, grab a shovel, cut away areas that are raised, and fill in those that are depressed.</p>
                                    <p>Another issue that plagues lawns, particularly in high-traffic areas, is soil compaction. This occurs when the soil becomes densely packed, making it difficult for grass to take root and allowing hardier weeds to take over. To test your yard for this problem, stick a garden fork into the ground. If the tines fail to penetrate 2 inches (5.08 centimeters), your soil is compacted and should be loosened with an aerator designed to remove small plugs of soil from your lawn.</p>
                                    <p>Even if the soil is properly prepared, you can still have a problem with thatch, a tangle of above-ground roots common in dense, spreading grasses like Bermuda and Zoysia. In especially bad cases, a thick mat of thatch can make it difficult for water and nutrients to reach the soil. You can break up thatch with a specially designed rake or with a mechanized dethatcher for larger jobs.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="page-bot"><img src="<?php echo BASE_URL();?>assets/images/under-footer.png" class="img-responsive" alt=""></div>
    </div>
    <?php include "includes/footer.php";?>
    </body>
    </html>