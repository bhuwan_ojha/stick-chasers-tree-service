<?php include "includes/header.php";?>
<div class="wrapper">
    <?php include "includes/nav-bar.php";?>
    <div id="pageContent" class="page-content">
        <section class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
                    <li><a href="<?php echo BASE_URL();?>home">Home</a></li>
                    <li class="active">Services</li>
                </ol>
            </div>
        </section>
        <section class="content content--fill content--fill--light top-null">
            <div class="container">
                <h1 class="text-center lined">Lawn Care Services</h1>
                <p class="text-center info-text">Lawn Care, is a locally owned and operated lawn care company providing environmentally responsible, lawn fertilization, weed and pest management control. With many years of experience when it comes to quality lawn care, our highly skilled team of lawn care professionals will handle any weed, diseases, pests infestation and rejuvenate your lawn back to it’s glory days</p>
                <div class="row">
                    <div class="col-md-4">
                        <div class="category animation" data-animation="fadeInLeft" data-animation-delay="0s">
                            <a href="#" class="category__image">
                                <figure><img src="<?php echo BASE_URL();?>assets/images/img-service-1.jpg" alt="" class="img-responsive" /></figure>
                            </a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Lawn Service</a>
                            <div class="category__text">
                                <p>We offer total lawn care and routine maintenance to keep your lawn looking brand new</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="category animation" data-animation="fadeIn" data-animation-delay="0.5s">
                            <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-2.jpg" alt="" class="img-responsive" /></a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Tree Service</a>
                            <div class="category__text">
                                <p>A professional arborist can work with you to safely and efficiently remove your tree</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="category animation last" data-animation="fadeInRight" data-animation-delay="0s">
                            <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-3.jpg" alt="" class="img-responsive" /></a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Landscape Design Services </a>
                            <div class="category__text">
                                <p>We provide a customer service driven business to develop custom outdoor living environments and management plans</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="category animation" data-animation="fadeInLeft" data-animation-delay="0s">
                            <a href="#" class="category__image">
                                <figure><img src="<?php echo BASE_URL();?>assets/images/img-service-4.jpg" alt="" class="img-responsive" /></figure>
                            </a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Leaf Removal</a>
                            <div class="category__text">
                                <p>Our crew blows leaves to an area of your choice ie: woods, garden, or compost pile</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="category animation" data-animation="fadeIn" data-animation-delay="0.5s">
                            <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-5.jpg" alt="" class="img-responsive" /></a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Snow Removal</a>
                            <div class="category__text">
                                <p>Snow plowing and removal priorities are based on traffic volume, usage and location within the transportation system </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="category animation last" data-animation="fadeInRight" data-animation-delay="0s">
                            <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-6.jpg" alt="" class="img-responsive" /></a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Junk Removal</a>
                            <div class="category__text">
                                <p>Our junk removal services provide a fast and easy solution for clearing out clutter and downsizing your space at a price you can afford</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="category animation" data-animation="fadeInLeft" data-animation-delay="0s">
                            <a href="#" class="category__image">
                                <figure><img src="<?php echo BASE_URL();?>assets/images/img-service-7.jpg" alt="" class="img-responsive" /></figure>
                            </a>
                            <div class="clearfix"></div> <a href="#" class="category__title">New Tree Planting</a>
                            <div class="category__text">
                                <p>You can trust our team to determine the best tree for your yard or transplant existing trees to keep your relationship going for years to come</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="category animation" data-animation="fadeIn" data-animation-delay="0.5s">
                            <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-8.jpg" alt="" class="img-responsive" /></a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Sod Service</a>
                            <div class="category__text">
                                <p>Sod offers a fast and effective solution to dead, dying and/or unhealthy grass </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="category animation last" data-animation="fadeInRight" data-animation-delay="0s">
                            <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-9.jpg" alt="" class="img-responsive" /></a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Retaining Walls</a>
                            <div class="category__text">
                                <p>We can work with you to decide what will be best by using all types of materials that are offered</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="category animation" data-animation="fadeInLeft" data-animation-delay="0s">
                            <a href="#" class="category__image">
                                <figure><img src="<?php echo BASE_URL();?>assets/images/img-service-10.jpg" alt="" class="img-responsive" /></figure>
                            </a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Patio and Fire Pit Pavers</a>
                            <div class="category__text">
                                <p>We will design and build a your custom outdoor living area with quality materials and superior installation</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="category animation" data-animation="fadeIn" data-animation-delay="0.5s">
                            <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-11.jpg" alt="" class="img-responsive" /></a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Yard Work</a>
                            <div class="category__text">
                                <p>We offer weekly, bi-weekly and monthly maintenance services for residential and commercial properties</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="category animation last" data-animation="fadeInRight" data-animation-delay="0s">
                            <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-12.jpg" alt="" class="img-responsive" /></a>
                            <div class="clearfix"></div> <a href="#" class="category__title">Excavation</a>
                            <div class="category__text">
                                <p>Excavating is known for meeting uniquely engineered and environmentally challenging excavating project design specifications</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="page-bot"><img src="<?php echo BASE_URL();?>assets/images/under-footer.png" class="img-responsive" alt=""></div>
    </div>
    <?php include "includes/footer.php";?>
    </body>
    </html>