<?php include "includes/header.php";?>
<div class="wrapper">
		<?php include "includes/nav-bar.php";?>
	
		<div id="pageContent" class="page-content">
			<section class="content" id="slider">
				<div class="tp-banner-container">
					<div class="tp-banner">
						<ul>
							<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
								<img src="<?php BASE_URL();?>assets/images/slides/slide-1.jpg" alt="slide1" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
								<div class="tp-caption tp-caption--wd-1  lfb ltt" data-x="540" data-y="255" data-speed="600" data-start="800" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 1;">We Work Hard</div>
								<div class="tp-caption tp-caption--wd-2 lfb ltt" data-x="570" data-y="320" data-speed="600" data-start="900" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 2;">on Your Yard</div>
								<div class="tp-caption tp-caption--wd-3 lfb ltt" data-x="600" data-y="410" data-speed="630" data-start="1000" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 3;">Work of exceptional quality since 1995</div>
							</li>
							<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
								<img src="<?php BASE_URL();?>assets/images/slides/slide-4.jpg" alt="slide4" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">

								<div class="tp-caption tp-caption--wd-1  lfb ltt" data-x="center" data-y="center" data-voffset="-100" data-hoffset="0" data-speed="600" data-start="800" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 1;">LAWN SERVICE</div>
								<div class="tp-caption tp-caption--wd-3 lfb ltt" data-x="center" data-y="center" data-voffset="-50" data-hoffset="0" data-speed="600" data-start="900" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 2;">Our revive, sustain & thrive services ensure the health of your lawn</div>
								<a href="#" class="tp-caption lfb ltt linkbtn estimator" data-x="center" data-y="center" data-voffset="30" data-hoffset="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 3;">Free Estimate</a> </li>
							<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
								<img src="<?php BASE_URL();?>assets/images/slides/slide-2.jpg" alt="slide2" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
								<div class="tp-caption tp-caption--wd-1  lfb ltt" data-x="center" data-y="center" data-voffset="-100" data-hoffset="0" data-speed="600" data-start="800" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 1;">landscape design</div>
								<div class="tp-caption tp-caption--wd-3 lfb ltt" data-x="center" data-y="center" data-voffset="-50" data-hoffset="0" data-speed="600" data-start="900" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 2;">custom landscape maintenance to complement and enhance your home</div>
								<a href="#" class="tp-caption lfb ltt linkbtn estimator" data-x="center" data-y="center" data-voffset="30" data-hoffset="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 3;">Free Estimate</a> </li>
							<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
								<img src="<?php BASE_URL();?>assets/images/slides/slide-3.jpg" alt="slide3" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
								<div class="tp-caption tp-caption--wd-1  lfb ltt" data-x="center" data-y="center" data-voffset="-100" data-hoffset="0" data-speed="600" data-start="800" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 1;">SNOW REMOVAL</div>
								<div class="tp-caption tp-caption--wd-3 lfb ltt" data-x="center" data-y="center" data-voffset="-50" data-hoffset="0" data-speed="600" data-start="900" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 2;">Avoid condo fees, let us take care of your home snow removal for less</div>
								<a href="#" class="tp-caption lfb ltt linkbtn estimator" data-x="center" data-y="center" data-voffset="30" data-hoffset="0" data-speed="600" data-start="1000" data-easing="Power4.easeOut" data-endeasing="Power4.easeIn" style="z-index: 3;">Free Estimate</a> </li>
						</ul>
					</div>
				</div>
			</section>

			<section class="content content--fill content--fill--light top-null">
				<div class="container">
					<div class="services-block">
						<div class="service iconmove dark">
							<div class="service-image"><img src="<?php BASE_URL();?>assets/images/service-1-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-icon"><i class="icon first icon-gardener"></i></div>
										<div class="service-title">Lawn Service</div>
										<div class="service-text">We offer total lawn care and routine maintenance to keep your lawn looking brand new</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove light ">
							<div class="service-image image-scale"><img src="<?php BASE_URL();?>assets/images/service-2-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-title">Landscape
											<br> Design Services </div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove light">
							<div class="service-image"><img src="<?php BASE_URL();?>assets/images/service-3-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-icon"><i class="icon first icon-tree"></i></div>
										<div class="service-title">Leaf Removal</div>
										<div class="service-text">Our crew blows leaves to an area of your choice ie: woods, garden, or compost pile</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove light">
							<div class="service-image image-scale"><img src="<?php BASE_URL();?>assets/images/service-4-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-title">Snow Removal</div>
										<div class="service-text">We don’t miss a beat when it comes to digging your home or office out from under the snow.</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove color">
							<div class="service-image"><img src="<?php BASE_URL();?>assets/images/service-5-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-icon"><i class="icon first icon-truck-transport"></i></div>
										<div class="service-title">Junk Removal</div>
									</div>
								</div>
							</div>
						</div>
						<div class="service iconmove light">
							<div class="service-image image-scale"><img src="<?php BASE_URL();?>assets/images/service-6-bg.jpg" alt="#"></div>
							<div class="caption">
								<div class="vert-wrap">
									<div class="vert">
										<div class="service-title">New Tree Planting</div>
										<div class="service-text">Let us help you protect your investment and let your new trees thrive!</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</section>
            <section class="content content--fill content--fill--light top-null" id="about-us-section">
                <div class="container">
                    <h1 class="text-center lined">About Us</h1>
                    <p class="text-center info-text">Starting out with just a single truck and mower, we have expanded our services and grown into one of the largest lawn maintenance companies in our area. Our expansion and stellar reputation is due, in part, to our exceptional reputation for quality and timely service. Our lawn care technicians utilize the latest technology and techniques to deliver beautiful results that will stand the test of time.</p>
                    <div class="row">
                        <div class="col-sm-6 animation" data-animation="fadeInLeft" data-animation-delay="0.5s">
                            <div class="text-icon__title">Our Mission</div>
                            <div class="text-icon last">
                                <div class="text-icon__icon"><i class="icon icon-nature-2"></i></div>
                                <div class="text-icon__info">
                                    <p>Our mission is to provide our customers with the highest level of quality services. We pledge to establish lasting relationships with our clients by exceeding their expectations and gaining their trust through exceptional performance. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 animation" data-animation="fadeInRight" data-animation-delay="0.5s">
                            <div class="text-icon__title">Our Clients</div>
                            <div class="text-icon last">
                                <div class="text-icon__icon"><i class="icon icon-construction-worker"></i></div>
                                <div class="text-icon__info">
                                    <p>Our clients count on our dependability, our drive, and our integrity and we take great pride in our accomplishments and build on them every day.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider divider--md"></div>
                    <h2 class="text-center lined">Our Advantages</h2>
                    <div class="row">
                        <div class="col-sm-4"><img src="<?php echo BASE_URL();?>assets/images/img-about-1.jpg" class="img-responsive" alt=""></div>
                        <div class="col-sm-4"><img src="<?php echo BASE_URL();?>assets/images/img-about-2.jpg" class="img-responsive" alt=""></div>
                        <div class="col-sm-4"><img src="<?php echo BASE_URL();?>assets/images/img-about-3.jpg" class="img-responsive" alt=""></div>
                    </div>
                    <div class="divider divider--md"></div>
                    <div class="text-center">
                        <p>We are a full service landscape maintenance company who commit to fulfilling all your landscape and lawn related needs. Our team of experts have over 28 years of experience in the landscape industry, which helps us develop and deliver custom made programs and solutions for you and your lawn. Through constant communication between our lawn care, landscape maintenance, and irrigation managers, we are able to provide you with a solution for any landscape. Our maintenance team completes a full property analysis during each visit and resolves any landscape related issues immediately to ensure your lawn is at its healthiest and looks its best. Lawn Care provides professional and quality landscaping services for both residential and commercial properties. From start to finish, we offer a wide range of lawn care services to accommodate your needs every step along the way.</p>
                        <p>We take pride in providing customers satisfaction that is second-to-none. Our team of professionals is fully licensed, insured, and well trained to provide you with consistent and high-quality value and services.</p>
                    </div>
                    <div class="divider divider--md"></div>
                    <h2 class="text-center lined">Lawn Care Team</h2>
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="person animation" data-animation="fadeInLeft" data-animation-delay="0.3s">
                                <div class="person__image"><img src="<?php echo BASE_URL();?>assets/images/person-01.jpg" alt="" class="img-responsive" /></div>
                                <div class="person__title">Mark Ronson</div>
                                <div class="person__position">Customer Service Manager</div>
                                <div class="person__text">
                                    <p>He joined our team 3 years ago and we are excited to have him as our lead technician</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="person animation" data-animation="fadeInLeft" data-animation-delay="0s">
                                <div class="person__image"><img src="<?php echo BASE_URL();?>assets/images/person-02.jpg" alt="" class="img-responsive" /></div>
                                <div class="person__title">Christopher Stoudt</div>
                                <div class="person__position">Fertilizer Technician</div>
                                <div class="person__text">
                                    <p>Over 6 years of experience in the lawn care industry and an interest in organic solutions</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="person animation" data-animation="fadeInRight" data-animation-delay="0s">
                                <div class="person__image"><img src="<?php echo BASE_URL();?>assets/images/person-03.jpg" alt="" class="img-responsive" /></div>
                                <div class="person__title">Joe Saboe</div>
                                <div class="person__position">Technician</div>
                                <div class="person__text">
                                    <p>He has worked as a certified technician for the past 7 years</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="person animation last" data-animation="fadeInRight" data-animation-delay="0.3s">
                                <div class="person__image"><img src="<?php echo BASE_URL();?>assets/images/person-04.jpg" alt="" class="img-responsive" /></div>
                                <div class="person__title">Alisa Madden</div>
                                <div class="person__position">Administrative Assistant</div>
                                <div class="person__text">
                                    <p>She is always available for customer's needs and answers the phone with a smile</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider divider--md"></div>
                   <!-- <div class="text-center"><a class="btn btn--wd">MEET ALL TEAM</a></div>-->
                </div>
            </section>
			<section class="content content--parallax top-null" data-image="<?php BASE_URL();?>assets/images/parallax-bg.jpg">
				<div class="container">
					<h2 class="text-center white lined">Our Difference</h2>
					<div class="iconcircle-wrap">
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-transport"></i></span> </div>
							<div class="iconcircle__title"> Clean, Branded Vehicles </div>
						</div>
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-people"></i></span> </div>
							<div class="iconcircle__title"> Professional, Uniformed Personnel </div>
						</div>
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-phone-auricular"></i></span> </div>
							<div class="iconcircle__title"> Timely Response Guarantee </div>
						</div>
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-lawn-mowet"></i></span> </div>
							<div class="iconcircle__title"> Safe, Reliable Equipment Maintained Daily </div>
						</div>
						<div class="iconcircle iconmove">
							<div class="iconcircle__icon"> <span><i class="icon icon-analytic-report"></i></span> </div>
							<div class="iconcircle__title"> Status and Quality Reports Delivered Timely </div>
						</div>
					</div>
				</div>
			</section>
    <section class="content content--fill content--fill--light top-null" id="services-section">
        <div class="container">
            <h1 class="text-center lined">Lawn Care Services</h1>
            <p class="text-center info-text">Lawn Care, is a locally owned and operated lawn care company providing environmentally responsible, lawn fertilization, weed and pest management control. With many years of experience when it comes to quality lawn care, our highly skilled team of lawn care professionals will handle any weed, diseases, pests infestation and rejuvenate your lawn back to it’s glory days</p>
            <div class="row">
                <div class="col-md-4">
                    <div class="category animation" data-animation="fadeInLeft" data-animation-delay="0s">
                        <a href="#" class="category__image">
                            <figure><img src="<?php echo BASE_URL();?>assets/images/img-service-1.jpg" alt="" class="img-responsive" /></figure>
                        </a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Hazardous Tree Removal</a>
                        <div class="category__text">
                            <p>We offer total lawn care and routine maintenance to keep your lawn looking brand new</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category animation" data-animation="fadeIn" data-animation-delay="0.5s">
                        <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-2.jpg" alt="" class="img-responsive" /></a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Tree Prunings</a>
                        <div class="category__text">
                            <p>A professional arborist can work with you to safely and efficiently remove your tree</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category animation last" data-animation="fadeInRight" data-animation-delay="0s">
                        <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-3.jpg" alt="" class="img-responsive" /></a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Weight Reduction </a>
                        <div class="category__text">
                            <p>We provide a customer service driven business to develop custom outdoor living environments and management plans</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="category animation" data-animation="fadeInLeft" data-animation-delay="0s">
                        <a href="#" class="category__image">
                            <figure><img src="<?php echo BASE_URL();?>assets/images/img-service-4.jpg" alt="" class="img-responsive" /></figure>
                        </a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Dead Wooding</a>
                        <div class="category__text">
                            <p>Our crew blows leaves to an area of your choice ie: woods, garden, or compost pile</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category animation" data-animation="fadeIn" data-animation-delay="0.5s">
                        <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-5.jpg" alt="" class="img-responsive" /></a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Selective Thining</a>
                        <div class="category__text">
                            <p>Snow plowing and removal priorities are based on traffic volume, usage and location within the transportation system </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category animation last" data-animation="fadeInRight" data-animation-delay="0s">
                        <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-6.jpg" alt="" class="img-responsive" /></a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Shape & Appearance</a>
                        <div class="category__text">
                            <p>Our junk removal services provide a fast and easy solution for clearing out clutter and downsizing your space at a price you can afford</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="category animation" data-animation="fadeInLeft" data-animation-delay="0s">
                        <a href="#" class="category__image">
                            <figure><img src="<?php echo BASE_URL();?>assets/images/img-service-7.jpg" alt="" class="img-responsive" /></figure>
                        </a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Hard Prunings of Shrubs & Ornamentals</a>
                        <div class="category__text">
                            <p>You can trust our team to determine the best tree for your yard or transplant existing trees to keep your relationship going for years to come</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category animation" data-animation="fadeIn" data-animation-delay="0.5s">
                        <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-8.jpg" alt="" class="img-responsive" /></a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Lot Clearing / Bush Removal</a>
                        <div class="category__text">
                            <p>Sod offers a fast and effective solution to dead, dying and/or unhealthy grass </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category animation last" data-animation="fadeInRight" data-animation-delay="0s">
                        <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-9.jpg" alt="" class="img-responsive" /></a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Stump Grinding</a>
                        <div class="category__text">
                            <p>We can work with you to decide what will be best by using all types of materials that are offered</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="category animation" data-animation="fadeInLeft" data-animation-delay="0s">
                        <a href="#" class="category__image">
                            <figure><img src="<?php echo BASE_URL();?>assets/images/img-service-10.jpg" alt="" class="img-responsive" /></figure>
                        </a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Seasoned Split Firewood Sales</a>
                        <div class="category__text">
                            <p>We will design and build a your custom outdoor living area with quality materials and superior installation</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category animation" data-animation="fadeIn" data-animation-delay="0.5s">
                        <a href="#" class="category__image"><img src="<?php echo BASE_URL();?>assets/images/img-service-11.jpg" alt="" class="img-responsive" /></a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Snow Plowing</a>
                        <div class="category__text">
                            <p>We offer weekly, bi-weekly and monthly maintenance services for residential and commercial properties</p>
                        </div>
                    </div>
                </div>
                <!--<div class="col-md-4">
                    <div class="category animation last" data-animation="fadeInRight" data-animation-delay="0s">
                        <a href="#" class="category__image"><img src="<?php /*echo BASE_URL();*/?>assets/images/img-service-12.jpg" alt="" class="img-responsive" /></a>
                        <div class="clearfix"></div> <a href="#" class="category__title">Excavation</a>
                        <div class="category__text">
                            <p>Excavating is known for meeting uniquely engineered and environmentally challenging excavating project design specifications</p>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </section>
            <section class="content content--fill content--fill--light top-null" id="about-us-section">
                <div class="container">
                    <h1 class="text-center lined">Testimonials</h1>
                    <section class="quotes">
                        <div class="bubble">
                            <blockquote>I refuse to accept the view that mankind is so tragically bound to the starless midnight of racism and war that the bright daybreak of peace and brotherhood can never become a reality... I believe that unarmed truth and unconditional love will have the final word.
                            </blockquote>
                            <div></div>
                            <cite> Martin Luther King, Jr.</cite> </div>
                        <div class="bubble">
                            <blockquote> Peace is not a relationship of nations. It is a condition of mind brought about by a serenity of soul. Peace is not merely the absence of war. It is also a state of mind. Lasting peace can come only to peaceful people.
                            </blockquote>
                            <div></div>
                            <cite> Jawaharlal Nehru</cite> </div>
                        <div class="bubble">
                            <blockquote> Forgiveness is not always easy. At times, it feels more painful than the wound we suffered, to forgive the one that inflicted it. And yet, there is no peace without forgiveness.
                            </blockquote>
                            <div></div>
                            <cite> Marianne Williamson</cite> </div>
                        <div class="bubble">
                            <blockquote>Of all our dreams today there is none more important - or so hard to realise - than that of peace in the world. May we never lose our faith in it or our resolve to do everything that can be done to convert it one day into reality. </blockquote>
                            <div></div>
                            <cite> Lester B. Pearson</cite> </div>
                    </section>
                </div>
            </section>
			<section class="content content--fill content--fill--light top-null">
				<div class="container">
					<div class="row">
						<div class="col-md-8 animation" data-animation="fadeIn" data-animation-delay="0.6s">
							<h2 class="text-center lined">Video Presentation </h2>
							<div class="video-responsive">
								<iframe src="https://www.youtube.com/embed/-PWxUQCNGtg?wmode=opaque" allowfullscreen></iframe>
							</div>
						</div>
						<div class="divider divider--md visible-sm visible-xs"></div>
						<div class="col-md-4 animation" data-animation="fadeIn" data-animation-delay="0.9s">
							<h2 class="text-center lined">Facebook</h2>
                            <div class="facebook-page">
                                <div class="fb-page" data-href="https://www.facebook.com/Stick-Chasers-Tree-Service-728901497247114/" data-tabs="timeline" data-height="421" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>

                            </div>
					</div>
				</div>
			</section>
    <!--<section class="content content--fill content--fill--light top-null" id="maintenance-section">
        <div class="container">
            <h1 class="text-center lined">Maintenance Tips</h1>
            <div class="panel-group">
                <div class="faq-item">
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#faq1">
                                    Lawn Care Equipment Maintenance in the Spring
                                    <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                    <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                </a>
                            </h4> </div>
                        <div id="faq1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p>A little bit of service and preventative maintenance can ensure that your lawnmower, string trimmer and other implements are running efficiently and won't break down before you place them back into storage for the winter. Such machines are typically gasoline powered, so they should be serviced using the following simple steps:
                                <p>
                                <p><b>1. Remove the gasoline.</b> Leftover gasoline from the previous year can become stale, choking the carburetor and causing rust.
                                    <br> <b>2. Disconnect the spark plug. </b>This disables the engine, making it safer to perform service on the machine.
                                    <br> <b>3. Remove the blade.</b> This step applies mainly to lawnmowers, though other equipment like edgers also have blades. While this piece is removed, sharpen it using a metal file.
                                    <br> <b>4. Drain the oil. </b>This step doesn't apply to string trimmers and other handheld lawn machines, which typically have two-cycle engines and run on a mixture of gasoline and oil. Four-cycle engines, common on lawnmowers, will need to be drained of oil.
                                    <br> <b>5. Clean the equipment.</b> Use a putty knife and wire brush to knock off accumulated grass and mud, then reattach the blade if you removed one earlier.
                                    <br> <b>6. Fill the oil tank. </b>If you're servicing a four-cycle engine, refill the oil tank with fresh oil.
                                    <br> <b>7. Replace the air filter. </b>This improves airflow to the engine, allowing it to run more smoothly.
                                    <br> <b>8. Replace the spark plug.</b> Although your old spark plug may still work properly, installing a new one is a cheap and easy way to ensure optimal performance.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="faq-item">
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#faq2" class="collapsed">
                                    How to Mow Your Lawn in the Spring
                                    <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                    <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                </a>
                            </h4> </div>
                        <div id="faq2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>It may surprise you that there's more to grass cutting than cranking up the lawnmower and pushing it across the lawn. Both mowing height and frequency are important to the health of your grass.</p>
                                <p>Though it may reduce the number of times you have to mow, cutting your grass short is harmful to your lawn in the long run. Mowing with a low blade height removes nutrients stored in leaf blades and exposes the soil to sunlight, allowing weeds to take hold more easily. Taller grass is better able to compete with weeds, thanks to a larger root system and a higher tolerance for heat. It also shades the ground, allowing the soil to retain water more effectively.</p>
                                <p>Given these benefits, it's a good idea to cut your grass at the tallest height recommended for your grass type, which are as follows: </p>
                                <ul class="marker-list">
                                    <li>Common bermudagrass: 1-2 inches (2.5-5 centimeters)</li>
                                    <li>Fescue: 2-3.5 inches (5-9 centimeters)</li>
                                    <li>Kentucky bluegrass: 2-3.5 inches (5-9 centimeters)</li>
                                    <li>St. Augustine: 2-4 inches (5-10 centimeters)</li>
                                    <li>Zoysia: 0.5-1.5 inches (1-4 centimeters)</li>
                                </ul>
                                <p>Mow your lawn often enough so that you're only removing the top one-third of the blades. This places less stress on the grass, and the smaller clippings are able to decompose more easily. Avoid bagging these clippings; this added organic matter is actually quite good for the soil.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="faq-item">
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#faq3" class="collapsed">
                                    Fertilizing Grass in the Spring
                                    <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                    <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                </a>
                            </h4> </div>
                        <div id="faq3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Once your grass is well-established, you can encourage its growth and discourage weeds by applying a combination of fertilizers and herbicides. When you should apply these substances and how much you should apply depends on several factors, like where you live and the extent of your weed problem.</p>
                                <p>Fertilizer can help your lawn grow thick and lush, but if it's not used properly, it can actually damage the grass. A slow-release nitrogen fertilizer is best, and no more than 1 pound (0.45 kilograms) of nitrogen should be spread per 1,000 square feet (93 square meters). It should be applied early in the season when the turf begins actively growing, so the timing varies among regions. Fertilizer should not be applied too early or late, however, as lingering cold or early heat can stress the grass. Check the packaging to see when and how much you should water after applying the fertilizer.</p>
                                <p>Herbicides must also be used with care, as their effectiveness often depends on when they're used. If you have a widespread weed infestation, it's best to apply a pre-emergent herbicide to your lawn before the seeds germinate in the spring. Be aware, however, that you can't use this treatment if you plan to plant new grass, as the herbicide will also prevent those seeds from germinating. For more isolated problems, spot treating with a non-selective herbicide should be enough to do the trick. Ultimately, the best way to discourage weeds is to have a thick, healthy lawn.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="faq-item">
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#faq4" class="collapsed">
                                    Planting Grass in the Spring
                                    <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                    <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                </a>
                            </h4> </div>
                        <div id="faq4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Once you've cleaned and repaired your lawn, you may need to reseed parts of it that are particularly bare or brown. This can dramatically improve the appearance of your grass, but there are a few simple steps you should follow to ensure that it won't look worse after you plant than it did before.</p>
                                <p>First try to address the soil conditions that prevented grass from growing in the past. Call your local Cooperative Extension office to find out where you can get a soil test; this will tell you what nutrients your lawn is lacking. Once you've corrected your soil composition, aerate the ground to avoid any problems with soil compaction.</p>
                                <p>Now you're ready to buy seed and spread it on your lawn. Before choosing a seed, determine which varieties will work best in your region of the country and with the amount of sunlight in your yard. Then roughly estimate the size of the area where you plan to plant, as seed coverage is recommended in pounds per square foot. If you're spreading the seed over a large area, it is best to use a broadcast spreader, but smaller areas can be seeded by hand.</p>
                                <p>Don't ignore the grass once you've planted it. Water regularly to maintain soil moisture and fertilize with a slow-release, low-nitrogen product. Mow when the grass reaches 3 or 4 inches (7.6 to 10 centimeters) in height, but try not to trim off more than a half-inch (1-centimeter) as doing so could stress the plant.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="faq-item">
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#faq5" class="collapsed">
                                    Cleaning and Repairing Your Lawn in Spring
                                    <span class="caret-toggle closed"><i class="icon icon-minus"></i></span>
                                    <span class="caret-toggle opened"><i class="icon icon-plus"></i></span>
                                </a>
                            </h4> </div>
                        <div id="faq5" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>If your lawn is already well-maintained, all you need to do is give it a light raking once the ground has dried out. However, problem areas should be addressed quickly, as they can stress your lawn and make it more susceptible to weeds and disease.</p>
                                <p>One common problem is uneven ground. Low spots cause poor drainage, while high spots are often scalped by the lawn mower. Since these situations create poor growing conditions for grass, grab a shovel, cut away areas that are raised, and fill in those that are depressed.</p>
                                <p>Another issue that plagues lawns, particularly in high-traffic areas, is soil compaction. This occurs when the soil becomes densely packed, making it difficult for grass to take root and allowing hardier weeds to take over. To test your yard for this problem, stick a garden fork into the ground. If the tines fail to penetrate 2 inches (5.08 centimeters), your soil is compacted and should be loosened with an aerator designed to remove small plugs of soil from your lawn.</p>
                                <p>Even if the soil is properly prepared, you can still have a problem with thatch, a tangle of above-ground roots common in dense, spreading grasses like Bermuda and Zoysia. In especially bad cases, a thick mat of thatch can make it difficult for water and nutrients to reach the soil. You can break up thatch with a specially designed rake or with a mechanized dethatcher for larger jobs.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <!--<section class="content content--fill content--fill--light top-null" id="pricing-section">
        <div class="container">
            <h1 class="text-center lined">Our Prices</h1>
            <div class="table-responsive">
                <table class="table price-table">
                    <tbody>
                    <tr>
                        <th colspan="2">
                            <p class="price-info">Choose one of our award-winning programs for automatically-scheduled lawn maintenance and a lush, green lawn.</p>
                        </th>
                        <th><span class="price-title"><i class="icon icon-wheelbarrow"></i>Simple Program</span></th>
                        <th><span class="price-title-color"><i class="icon icon-landkeeper"></i>Extended Program</span></th>
                    </tr>
                    <tr>
                        <td><img src="<?php /*echo BASE_URL();*/?>assets/images/pricing-1.jpg" class="img-responsive" alt=""></td>
                        <td>Season-Long Fertilization</td>
                        <td class="text-center"><i class="icon icon-check"></i></td>
                        <td class="text-center"><i class="icon icon-check color"></i></td>
                    </tr>
                    <tr>
                        <td><img src="<?php /*echo BASE_URL();*/?>assets/images/pricing-2.jpg" class="img-responsive" alt=""></td>
                        <td>Season-Long Weed Control</td>
                        <td class="text-center"><i class="icon icon-check"></i></td>
                        <td class="text-center"><i class="icon icon-check color"></i></td>
                    </tr>
                    <tr>
                        <td><img src="<?php /*echo BASE_URL();*/?>assets/images/pricing-3.jpg" class="img-responsive" alt=""></td>
                        <td>Lawn Analysis With Every Visit</td>
                        <td class="text-center"><i class="icon icon-check"></i></td>
                        <td class="text-center"><i class="icon icon-check color"></i></td>
                    </tr>
                    <tr>
                        <td><img src="<?php /*echo BASE_URL();*/?>assets/images/pricing-4.jpg" class="img-responsive" alt=""></td>
                        <td>IPM Surface Insect Control</td>
                        <td class="text-center"></td>
                        <td class="text-center"><i class="icon icon-check color"></i></td>
                    </tr>
                    <tr>
                        <td><img src="<?php /*echo BASE_URL();*/?>assets/images/pricing-5.jpg" class="img-responsive" alt=""></td>
                        <td>Mechanical Core Aeration</td>
                        <td class="text-center"></td>
                        <td class="text-center"><i class="icon icon-check color"></i></td>
                    </tr>
                    <tr>
                        <td><img src="<?php /*echo BASE_URL();*/?>assets/images/pricing-6.jpg" class="img-responsive" alt=""></td>
                        <td>FREE! Over-Seeding Upgrade</td>
                        <td class="text-center"></td>
                        <td class="text-center"><i class="icon icon-check color"></i></td>
                    </tr>
                    <tr class="actions">
                        <td colspan="2"><a href="#" class="btn btn--wd">Click here for an Instant Online Quote</a></td>
                        <td class="text-center"><a href="#" class="btn btn--wd">Order Now!</a></td>
                        <td class="text-center"><a href="#" class="btn btn--wd">Order Now!</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="divider divider--md"></div>
            <h2 class="text-center lined">Additional Services</h2>
            <p class="info-text text-center"> Thatch problem? Weeds in the interlock? We offer a variety of additional services that you can add to any program.</p>
            <div class="row">
                <div class="col-sm-3">
                    <div class="price-box">
                        <div class="title">
                            <div class="vert-wrap">
                                <div class="vert">Weed Control for Paths, Patios and Driveways</div>
                            </div>
                        </div>
                        <div class="text">Starting from just</div>
                        <div class="price">$19.95</div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="price-box">
                        <div class="title">
                            <div class="vert-wrap">
                                <div class="vert">Dethatching</div>
                            </div>
                        </div>
                        <div class="text">Starting from just</div>
                        <div class="price">$99.95</div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="price-box">
                        <div class="title">
                            <div class="vert-wrap">
                                <div class="vert">Dethatching +
                                    <br>Overseeding</div>
                            </div>
                        </div>
                        <div class="text">Starting from just</div>
                        <div class="price">$129.95</div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="price-box">
                        <div class="title">
                            <div class="vert-wrap">
                                <div class="vert">Granular Compost and Seed</div>
                            </div>
                        </div>
                        <div class="text">Starting from just</div>
                        <div class="price">$69.96</div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    <section class="content content--fill content--fill--light fullwidth top-null" id="gallery-section">
        <div class="container">
            <h1 class="text-center lined">Gallery</h1>
            <section class="filters-by-category">
                <div class="container">
                    <ul class="option-set" data-option-key="filter">
                        <li><a href="#filter" data-option-value="*" class="selected">All</a></li>
                        <li><a href="#filter" data-option-value=".category1" class="">Lawn Service</a></li>
                        <li><a href="#filter" data-option-value=".category2" class="">Landscape Design</a></li>
                        <li><a href="#filter" data-option-value=".category3" class="">Leaf Removal</a></li>
                        <li><a href="#filter" data-option-value=".category4" class="">Snow Removal</a></li>
                        <li><a href="#filter" data-option-value=".category5" class="">Junk Removal</a></li>
                        <li><a href="#filter" data-option-value=".category6" class="">Planting New Tree</a></li>
                    </ul>
                </div>
            </section>
            <div class="gallery gallery-isotope" id="gallery">
                <div class="gallery__item doubleW doubleH category1">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-01.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-01.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category2">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-02.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-02.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category1">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-03.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-03.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category2">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-04.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-04.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category3 category5">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-05.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-05.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category4">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-06.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-06.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category2">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-07.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-07.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category2">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-08.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-08.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category3">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-09.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-09.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category6">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-10.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-10.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item doubleW doubleH category3">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-14.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-14.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category4">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-11.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-11.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category1">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-12.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-12.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
                <div class="gallery__item category2">
                    <div class="gallery__item__image"> <img src="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-13.jpg" alt="" /> <a class="btn btn--round" href="<?php echo BASE_URL();?>assets/images/gallery/gallery-img-13.jpg"><span class="icon icon-magnifying-glass"></span></a> </div>
                </div>
            </div>
            <div class="divider divider--md"></div>
            <!--<div class="text-center"><a class="btn btn--wd view-more-gallery" href="<?php /*echo BASE_URL();*/?>gallery">More photos</a></div>-->
            <div id="galleryPreload"></div>
        </div>
    </section>

    <section class="content content--fill content--fill--light top-null bottom-null fullwidth" id="contact-section">
        <h2 class="text-center lined " data-toggle="collapse" data-target="#demo" ><span class="collabsable">Contact Us </span> </h2>

        <!--<div id="map"></div>-->
    </section>
    <section class="content content--fill content--fill--light top-null" id="demo">

        <div class="container">
            <div class="row">
                <div class="col-sm-1 visible-lg"></div>
                <div class="col-sm-3">
                    <div class="address">
                        <div class="pull-right"><img src="<?php echo BASE_URL();?>assets/images/logo.png" class="img-responsive" alt="" /></div>
                        <div class="divider divider--sm"></div>
                        <div class="text-right">
                            <h6>Our address:</h6> <?php echo SITE_STREET;?>
                            <br>   <?php echo SITE_CITY;?> , <?php echo SITE_STATE;?> <?php echo SITE_ZIP;?>
                             </div>
                        <div class="divider divider--sm"></div>
                        <div class="text-right">
                            <h6>Call us:</h6><?php echo SITE_NUMBER;?> </div>
                        <div class="divider divider--sm"></div>
                        <div class="text-right">
                            <h6>Have any questions?</h6> <a href="mailto:<?php echo SITE_EMAIL;?>"><?php echo SITE_EMAIL;?></a>
                            <br>  </div>
                        <div class="divider divider--md"></div>
                        <div class="social-links lg">
                            <ul>
                                <!--<li>
                                    <a class="icon icon-twitter-logo-button" href="#"></a>
                                </li>-->
                                <li>
                                    <a class="icon icon-facebook-logo-button" href="https://www.facebook.com/Stick-Chasers-Tree-Service-728901497247114/" target="_blank"></a>
                                </li>
                                <!-- <li>
                                     <a class="icon icon-instagram-logo" href="#"></a>
                                 </li>-->
                            </ul>
                            <ul>
                                <!-- <li>
                                     <a class="icon icon-google-plus" href="#"></a>
                                 </li>
                                 <li>
                                     <a class="icon icon-social" href="#"></a>
                                 </li>
                                 <li>
                                     <a class="icon icon-linkedin-logo-button" href="#"></a>
                                 </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1 visible-lg"></div>
                <div class="col-sm-7">
                    <form id="contactform" class="contact-form" name="contactform" method="post" novalidate>
                        <div id="success">
                            <p>Your message was sent successfully!</p>
                        </div>
                        <div id="error">
                            <p>Something went wrong, try refreshing and submitting the form again.</p>
                        </div>
                        <div class="inputs-col">
                            <div class="input-wrapper">
                                <label>Name <span class="required">*</span></label>
                                <input type="text" class="input--wd input--full" name="name" placeholder="Your name"> </div>
                            <div class="input-wrapper">
                                <label>Phone</label>
                                <input type="text" class="input--wd input--full" name="phone" placeholder="(555) 555-5555"> </div>
                            <div class="input-wrapper">
                                <label>Email</label>
                                <input type="text" class="input--wd input--full" name="email" placeholder="xxxx@xxxx.xxx"> </div>
                        </div>
                        <div>
                            <label>Message</label>
                            <textarea class="textarea--wd input--full" name="message"></textarea>
                        </div>
                        <div class="divider divider--xs"></div>
                        <button type="submit" id="submit" class="btn btn--wd">Send Message</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

		</div>
		<?php include "includes/footer.php";?>
    <script>
        $("#contact").click(function() {
            $('html, body').animate({
                scrollTop: $("#contact-section").offset().top
            }, 2000);
        });
        $("#contact-us").click(function() {
            $('html, body').animate({
                scrollTop: $("#contact-section").offset().top
            }, 2000);
        });
        $("#services").click(function() {
            $('html, body').animate({
                scrollTop: $("#services-section").offset().top
            }, 2000);
        });
        $(".estimator").click(function() {
            $('html, body').animate({
                scrollTop: $("#free-estimation").offset().top
            }, 2000);
        });

     /*   $("#pricing").click(function() {
            $('html, body').animate({
                scrollTop: $("#pricing-section").offset().top
            }, 2000);
        });*/
    /*    $("#maintenance").click(function() {
            $('html, body').animate({
                scrollTop: $("#maintenance-section").offset().top
            }, 2000);
        });*/
        $("#gallery").click(function() {
            $('html, body').animate({
                scrollTop: $("#gallery-section").offset().top
            }, 2000);
        });
        $("#about-us").click(function() {
            $('html, body').animate({
                scrollTop: $("#about-us-section").offset().top
            }, 2000);
        });
    </script>

    <!--<script src="https://maps.googleapis.com/maps/api/js?key=<?php /*echo GOOGLE_MAP_API_KEY; */?>&callback=initMap"></script>
<script type="text/javascript">

    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        var mapOptions = {
            zoom: 14
            , scrollwheel: false
            ,
            center: new google.maps.LatLng(<?php /*echo LATLNG;*/?>),

            styles: [{
                "featureType": "water"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#e9e9e9"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "landscape"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "road.highway"
                , "elementType": "geometry.fill"
                , "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "road.highway"
                , "elementType": "geometry.stroke"
                , "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 29
                }, {
                    "weight": 0.2
                }]
            }, {
                "featureType": "road.arterial"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 18
                }]
            }, {
                "featureType": "road.local"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "featureType": "poi"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 21
                }]
            }, {
                "featureType": "poi.park"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#dedede"
                }, {
                    "lightness": 21
                }]
            }, {
                "elementType": "labels.text.stroke"
                , "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "elementType": "labels.text.fill"
                , "stylers": [{
                    "saturation": 36
                }, {
                    "color": "#333333"
                }, {
                    "lightness": 40
                }]
            }, {
                "elementType": "labels.icon"
                , "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit"
                , "elementType": "geometry"
                , "stylers": [{
                    "color": "#f2f2f2"
                }, {
                    "lightness": 19
                }]
            }, {
                "featureType": "administrative"
                , "elementType": "geometry.fill"
                , "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "administrative"
                , "elementType": "geometry.stroke"
                , "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 17
                }, {
                    "weight": 1.2
                }]
            }]
        };

        var mapElement = document.getElementById('map');

        var map = new google.maps.Map(mapElement, mapOptions);
        var image = 'https://lh3.googleusercontent.com/-8PyfdDLP-_g/WMGabVb8zBI/AAAAAAAABoM/qujFkg8N9CYvrBedSphsXfXxX1-_lWKaQCL0B/h143/2017-03-09.png';

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(<?php /*echo LATLNG;*/?>)
            , map: map
            , icon: image
        });
    }

</script>-->
</div>
</body>
</html>