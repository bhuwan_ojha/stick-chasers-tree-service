<?php include "includes/header.php";?>
<div class="wrapper">
    <?php include "includes/nav-bar.php";?>
    <div id="pageContent">
        <section class="breadcrumbs  hidden-xs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
                    <li><a href="#">Home</a></li>
                    <li class="active">Sitemap</li>
                </ol>
            </div>
        </section>
        <section class="content content--fill content--fill--light top-null">
            <div class="container">
                <div class="row">
                    <h2 class="text-center lined">Site Map</h2>
                    <ul class="sitemap">
                        <li>
                            <ul>
                                <li><a href="<?php echo BASE_URL();?>home">Home</a></li>
                                <li><a href="<?php echo BASE_URL();?>about">About</a></li>
                                <li><a href="<?php echo BASE_URL();?>services">Services</a>
                                    <ul>
                                        <li><a href="#">lawn service</a>
                                            <ul>
                                                <li class="level2"><a href="#">trimming edge</a></li>
                                                <li class="level2"><a href="#">clean up</a></li>
                                                <li class="level2"><a href="#">blowing</a></li>
                                                <li class="level2"><a href="#">weeding</a></li>
                                                <li class="level2"><a href="#">moving</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">tree service</a></li>
                                        <li><a href="#">landscape design services</a></li>
                                        <li><a href="#">leaf removal</a></li>
                                        <li><a href="#">Snow Removal</a></li>
                                        <li><a href="#">Junk Removal</a></li>
                                        <li><a href="#">New tree planting</a></li>
                                        <li><a href="#">Sod service</a></li>
                                        <li><a href="#">Retaining walls</a></li>
                                        <li><a href="#">Patio and fire pit pavers</a></li>
                                        <li><a href="#">Yard work</a></li>
                                        <li><a href="#">Excavation</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo BASE_URL();?>pricing">Pricing</a></li>
                                <li><a href="<?php echo BASE_URL();?>gallery">Gallery</a></li>
                                <li><a href="<?php echo BASE_URL();?>maintenance">Maintenance tips</a></li>
                                <li><a href="<?php echo BASE_URL();?>contact">Contacts</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <div class="page-bot"><img src="<?php echo BASE_URL();?>assets/images/under-footer.png" class="img-responsive" alt=""></div>
    </div>
    <?php include "includes/footer.php";?>
    </body>
    </html>