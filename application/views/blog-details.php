<?php include "includes/header.php";?>
<div class="wrapper">
    <?php include "includes/nav-bar.php";?>
    <div id="pageContent" class="page-content">
        <section class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
                    <li><a href="<?php echo BASE_URL();?>home">Home</a></li>
                    <li class="active">Blog</li>
                </ol>
            </div>
        </section>
        <section class="content content--fill content--fill--light top-null">
            <div class="container">
                <h1 class="text-center lined">Blog Posts</h1>
                <div class="row">
                    <div class="col-md-8 column-center">
                        <div class="blog-post single">
                            <div class="post-image">
                                <a href="#"><img src="<?php echo BASE_URL();?>assets/images/blog/blog-post-single.jpg" alt=""></a>
                                <div class="post-image-over"></div>
                            </div>
                            <?php foreach ($blogDetails as $blogDetail):?>
                            <div class="post-content">
                                <h3 class="post-title"><?php echo $blogDetail['blog_title'];?></h3>
                                <div class="post-date"><span class="day" style="font-size: 25px!important;"><?php echo date("jS", strtotime($blogDetail['created_date']));?></span><span class="month"><?php echo date("F", strtotime($blogDetail['created_date']));?></span></div>
                                <div class="post-author">
                                    <div class="text-center"><img src="<?php echo BASE_URL();?>assets/images/blog/author-1.jpg" alt=""></div>
                                    <?php echo $blogDetail['created_by'];?>
                                </div>
                                <div class="post-teaser">
                                    <p><?php echo $blogDetail['blog_description'];?></p>
                                    <ul class="tags-list">
                                        <li><a href="#">Lawn care</a></li>
                                        <li><a href="#">Pennsylvania</a></li>
                                        <li><a href="#">Moisture</a></li>

                                    </ul>
                                </div>
                            </div>
                            <?php endforeach;?>

                        </div>
                        <div class="divider divider--md"></div>
                        <h5><b>COMMENTS</b></h5>
                            <?php foreach ($comment as $commentData):?>
                            <div class="comments">
                                <div class="comment">
                                    <div class="userpic"><img src="<?php echo BASE_URL();?>assets/images/blog/userpic.png" alt=""></div>
                                    <div class="text">
                                        <div class="meta"><?php echo $commentData['name'];?><span class="date"> <?php echo $commentData['commented_date'];?> </span></div>
                                        <p><?php echo $commentData['comment'];?></p>
                                        <!--<a href="#" class="btn btn--wd btn--sm">reply</a>-->
                                    </div>
                                </div>


                            <!--<div class="comment replay">
                                <div class="userpic"><img src="<?php /*echo BASE_URL();*/?>assets/images/blog/userpic.png" alt=""></div>
                                <div class="text">
                                    <div class="meta">Ryan, Manson MN <span class="date"> 10 / 4 / 2016 </span></div>
                                    <p>Fusce eu dui. Integer vel nibh sit amet turpis vulputate aliquet. Phasellus id nisi vitae odio pretium aliquam. Pellentesque a leo. Donec consequat lectus sed felis. Quisque vestibulum massa. </p>
                                    <a href="#" class="btn btn--wd btn--sm">reply</a>
                                </div>
                            </div>-->
                            </div>
                            <?php endforeach;?>
                        <div class="divider divider--sm"></div>
                        <h5><b>LEAVE A COMMENT</b></h5>
                        <div class="divider-xxs"></div>
                        <form id="commentform" class="contact-form" name="contactform"  novalidate="novalidate">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="inputs-col">
                                        <div class="input-wrapper">
                                            <label>Name <span class="required">*</span></label>
                                            <input type="text" class="input--wd input--full" id="name" name="name" placeholder="Your name"> </div>
                                        <div class="input-wrapper">
                                            <label>Email</label>
                                            <input type="text" class="input--wd input--full" id="email" name="email" placeholder="xxxx@xxxx.xxx"> </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <label>Message</label>
                                <textarea class="textarea--wd input--full" name="message" id="comment"></textarea>
                            </div>
                            <div class="divider divider--xs"></div>
                            <button type="button" id="submit" class="btn btn--wd">Leave Message</button>
                        </form>

                        <div class="divider divider-lg"></div>

                    </div>
                    <div class="col-md-4 column-right">
                        <div class="side-block">
                            <h4>Archives</h4>
                            <div class="calendar">
                                <div class="calendar__header">November 2016</div>
                                <table>
                                    <tr>
                                        <th>S</th>
                                        <th>M</th>
                                        <th>T</th>
                                        <th>W</th>
                                        <th>Th</th>
                                        <th>F</th>
                                        <th>S</th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>5</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>7</td>
                                        <td>8</td>
                                        <td>9</td>
                                        <td>10</td>
                                        <td>11</td>
                                        <td>12</td>
                                    </tr>
                                    <tr>
                                        <td>13</td>
                                        <td>14</td>
                                        <td>15</td>
                                        <td>16</td>
                                        <td class="selected">17</td>
                                        <td>18</td>
                                        <td>19</td>
                                    </tr>
                                    <tr>
                                        <td>20</td>
                                        <td>21</td>
                                        <td>22</td>
                                        <td>23</td>
                                        <td>24</td>
                                        <td>25</td>
                                        <td>26</td>
                                    </tr>
                                    <tr>
                                        <td>27</td>
                                        <td>28</td>
                                        <td>29</td>
                                        <td>30</td>
                                        <td>31</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                <div class="calendar__footer"><a href="#">« Sep</a></div>
                            </div>
                        </div>
                        <div class="side-block">
                            <h4>Post Categories</h4>
                            <ul class="marker-list">
                                <?php foreach ($blogDetails as $blogDetail):?>
                                <li><a href="#"><?php echo $blogDetail['blog_category'];?><span>(2)</span></a></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                        <div class="side-block">
                            <h4>Popular tags</h4>
                            <ul class="tags-list">
                                <li><a href="#">lawn care</a></li>
                                <li><a href="#">lawn services</a></li>
                                <li><a href="#">decoration</a></li>
                                <li><a href="#">grass</a></li>
                                <li><a href="#">green</a></li>
                                <li><a href="#">flowers</a></li>
                                <li><a href="#">garden tips</a></li>
                                <li><a href="#">bermuda grass</a></li>
                                <li><a href="#">maintaince</a></li>
                            </ul>
                        </div>
                        <div class="side-block">
                            <h4>Popular Posts</h4>
                            <div class="blog-post post-preview">
                                <div class="wrapper">
                                    <div class="post-image">
                                        <a href="#"><img src="<?php echo BASE_URL();?>assets/images/blog/blog-post-small-1.jpg" alt=""></a>
                                    </div>
                                    <div class="post-content">
                                        <div class="post-date-sm">10 / 4 / 2016</div>
                                        <h5 class="post-title"><a href="#">How to Get the Best Lawn Care Services</a></h5>
                                        <div class="post-author-sm">Ricky B. Brown</div>
                                    </div>
                                </div>
                                <div class="post-meta"><i class="icon icon-speech"></i><span>4</span></div>
                            </div>
                            <div class="blog-post post-preview">
                                <div class="wrapper">
                                    <div class="post-image">
                                        <a href="#"><img src="<?php echo BASE_URL();?>assets/images/blog/blog-post-small-2.jpg" alt=""></a>
                                    </div>
                                    <div class="post-content">
                                        <div class="post-date-sm">10 / 4 / 2016</div>
                                        <h5 class="post-title"><a href="#">Falling Behind: Lawn Care Services</a></h5>
                                        <div class="post-author-sm">Ricky B. Brown</div>
                                    </div>
                                </div>
                                <div class="post-meta"><i class="icon icon-speech"></i><span>4</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="page-bot"><img src="<?php echo BASE_URL();?>assets/images/under-footer.png" class="img-responsive" alt=""></div>
</div>
<?php include "includes/footer.php";?>
<script>
    $('#submit').on('click',function(){
       var name = $('#name').val();
       var email = $('#email').val();
       var message = $('#comment').val();
       var artical = '<?php echo segment(2);?>';
        if(name && email && message && artical !=""){
            debugger;
            $.ajax({
                url:'<?php echo BASE_URL();?>comment-submit',
                type:'POST',
                data:{name:name,email:email,message:message,artical:artical},
                success:function(data){
                    $('#commentform').resetForm();
                }
            })
        }
    });
</script>
</body>
</html>