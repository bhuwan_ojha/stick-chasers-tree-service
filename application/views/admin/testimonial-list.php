<?php include "includes/base.php"; ?>
<?php successflash();?>
<div class="content">
    <div class="content-header">
        <div class="leftside-content-header">
            <ul class="breadcrumbs">
                <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">Testimonial Manager</a></li>
            </ul>
        </div>
    </div>

    <div class="row animated fadeInRight">

        <div class="pull-right">
            <button class="btn btn-success" onclick="location.href='<?php echo BASE_URL(); ?>admin/testimonial-form'"><i class="glyphicon glyphicon-plus "></i> Add</button>
        </div>
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel-content">
                    <div class="table-responsive">
                        <table id="basic-table" class="table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Description</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($data != 0) {
                                foreach ($data as $value) {
                                    ?>
                                    <tr>
                                        <td id="firstNode"><?php echo $value['id']; ?></td>
                                        <td contenteditable="false"><?php echo $value['page_title']; ?></td>
                                        <td contenteditable="false"><?php echo $value['created_date']; ?></td>
                                        <td contenteditable="false">
                                            <button class="btn btn-info btn-xs"
                                                    onclick="location.href='<?php echo BASE_URL(); ?>admin/testimonialSaveUpdate/<?php echo $value['id']; ?>'"
                                                    id="editbtn"><i class="glyphicon glyphicon-pencil "></i></button>
                                            <button class="btn btn-danger btn-xs" onclick="deletetestimonial(this)" id="deletebtn">
                                                <i class="glyphicon glyphicon-trash"></i></button>
                                    </tr>




                                <?php }
                            } else { ?>

                                <td colspan="4">No Record Found</td>

                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(function(){
        $(".left-nav").find(".active").removeClass("active");
        $('.testimonial').addClass('active-item');
    });
    function deletetestimonial(thisObj) {
        var id = $(thisObj).closest('tr').find('td:first').text();
        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL()?>admin/testimonial/delete",
            data: {id: id},
            success: function (data) {
                location.reload();
            }
        });
    }


</script>