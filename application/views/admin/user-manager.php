<?php include "includes/base.php"; ?>
<?php successflash();?>
<div class="content">
    <div class="content-header">
        <div class="leftside-content-header">
            <ul class="breadcrumbs">
                <li><i class="fa fa-home" aria-hidden="true"></i><a href="#">User Manager</a></li>
            </ul>
        </div>
    </div>
    <div class="row animated fadeInRight">
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel-content">
                    <div class="table-responsive">
                        <table id="basic-table" class="table " cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Username</th>
                                <th>Email Address</th>
                                <th>Password</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id="firstNode">1</td>
                                <td contenteditable="false"><?php echo $data[0]['username']; ?></td>
                                <td contenteditable="false"><?php echo $data[0]['email']; ?></td>
                                <td contenteditable="false" id="password"><?php echo $data[0]['password']; ?></td>
                                <td contenteditable="false" id="createdDate"><?php echo $data[0]['created_date']; ?></td>
                                <td contenteditable="false">
                                    <button class="btn btn-success" onclick="update(this)" style="display: none" id="savebtn"><i class="glyphicon glyphicon-check"></i></button>
                                    <button class="btn btn-info" id="editbtn"><i class="glyphicon glyphicon-pencil"></i>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(function(){
        $(".left-nav").find(".active").removeClass("active");
        $('.user').addClass('active-item');
    });

    $(function () {
        $(this).attr('type', 'password');
        $('#editbtn').on('click',function () {
            var currentTD = $(this).parents('tr').find('td');
            if ($('#editbtn')) {
                currentTD = $(this).parents('tr').find('td');
                $.each(currentTD, function () {
                    $(this).prop('contenteditable', true);
                    $('#editbtn').css("display", "none");
                    $('#savebtn').css("display", "");
                });
            } else {
                $.each(currentTD, function () {
                    $(this).prop('contenteditable', false)
                });
            }
            $(this).closest('td').attr('contenteditable', false);
            $(this).parents('tr').find("td[id='firstNode']").attr('contenteditable', false);
            $(this).parents('tr').find("td[id='createdDate']").attr('contenteditable', false);
        });
    });


    function update(thisObj) {
        var id = $('table tr td').eq(0).text();
        var username = $('table tr td').eq(1).text();
        var email = $('table tr td').eq(2).text();
        var password = $('table tr td').eq(3).text();

        $.ajax({
            type: "POST",
            url: "<?php echo BASE_URL()?>admin/Login/UpdateUser",
            data: {id: id, username: username, email: email, password: password},
            success: function (data) {
                $('#editbtn').css("display", "");
                $('#savebtn').css("display", "none");
                location.reload();
            }
        });
    }
</script>