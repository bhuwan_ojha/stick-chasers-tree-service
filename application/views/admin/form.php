<?php
$isNew = true;
$saveUpdate = "Save";

if (segment(3) != '') {
    $isNew = false;
    $saveUpdate = "Update";
}

?>
<?php include('includes/base.php'); ?>
    <div class="content">
        <div class="content-header">
            <div class="leftside-content-header">
                <ul class="breadcrumbs">
                    <li><i class="fa fa-home" aria-hidden="true"></i><a href="#"></a>Page Manager | <?php echo ($isNew) ? 'Add' : 'Update' ?></li>
                </ul>
            </div>
        </div>
        <form id="page-form" action="<?php echo BASE_URL();?>admin/add-pages" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php if (isset($data)) {echo $data[0]['id'];} ?>">
        <div class="form-group">
            <div class="col-sm-12">
            <label for="placeholder"  class="control-label" >Page Title</label>
                <input type="text" name="page_title"   class="form-control" id="placeholder" placeholder="Page Title" value=" <?php if(isset($data)) {echo $data[0]['page_title'];} ?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
            <label for="placeholder" class="control-label">Page Description</label>
                <textarea name="editor1" id="editor1" rows="10" cols="80">
                    <?php if (isset($data)) {
                        echo $data[0]['page_description'];
                    } ?>
                </textarea>
                <script>
                    CKEDITOR.replace('editor1');
                </script>
            </div>
        </div>

        <div class="form-group" style="">
            <div class="col-sm-2">
            <label class="cotrol-label">Image Uploader</label>
                    <img src="<?php echo BASE_URL();?>uploads/<?php if(isset($data)){echo $data[0]['page_image'];}else{?>dummy.png<?php }?>" style="height: 100px;">

                <input type="file"  name="image" value="<?php if(isset($data)){echo $data[0]['page_image'];}?>">
                <button type="submit" class="btn btn-wide btn-success" style="margin-top: 15px;"><?php echo $saveUpdate;?></button>
            </div>


        </div>
        </form>
    </div>
<script>
    $(function(){
        $(".left-nav").find(".active").removeClass("active");
        $('.left-menu-page').removeClass('close-item');
        $('.left-menu-page').addClass('open-item');
        $('.page').addClass('active-item');
    });
</script>



</body>
</html>