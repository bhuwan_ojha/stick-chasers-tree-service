<!doctype html>
<html lang="en" class="fixed accounts sign-in">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title><?php echo SITE_NAME; ?></title>
    <link href="<?php echo BASE_URL();?>apple-touch-icon.png" rel="apple-touch-icon">

    <link href="<?php echo BASE_URL();?>apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76">

    <link href="<?php echo BASE_URL();?>apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120">

    <link href="<?php echo BASE_URL();?>apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152">

    <link href="<?php echo BASE_URL();?>apple-touch-icon-152x152.png" rel="apple-touch-icon-precomposed">

    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/vendor/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/vendor/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo BASE_URL();?>admin-assets/vendor/animate.css/animate.css">

    <?php include "admin-assets/stylesheets/css/style.php";?>
</head>

<body>
<div class="wrap">
    <div class="page-body animated slideInDown">

        <div class="logo">
            <img alt="logo" src="<?php echo BASE_URL();?>assets/images/logo.png" />
        </div>
        <div style="text-align: center;"><h2><?php echo SITE_NAME;?></h2></div>
        <div class="box">
            <div class="panel mb-none">

                <div class="panel-content bg-scale-0">
                    <?php failedflash();?>
                    <form action="<?php echo BASE_URL();?>admin/Auth" method="post">
                        <div class="form-group mt-md">
                            <span class="input-with-icon">
                                <input type="text" class="form-control" name="username" placeholder="Email">
                                <i class="fa fa-envelope"></i>
                            </span>
                        </div>
                        <div class="form-group">
                            <span class="input-with-icon">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                <i class="fa fa-key"></i>
                            </span>
                        </div>
                        <div class="form-group">
                            <div class="checkbox-custom checkbox-primary">
                                <input type="checkbox" id="remember-me" value="option1" checked>
                                <label class="check" for="remember-me">Remember me</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo BASE_URL();?>admin-assets/vendor/jquery/jquery-1.12.3.min.js"></script>
<script src="<?php echo BASE_URL();?>admin-assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL();?>admin-assets/javascripts/template-script.min.js"></script>
<script src="<?php echo BASE_URL();?>admin-assets/javascripts/template-init.min.js"></script>
</body>
</html>
