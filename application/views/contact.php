<?php include "includes/header.php";?>
<div class="wrapper">
    <?php include "includes/nav-bar.php";?>
    <div id="pageContent" class="page-content">
        <section class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
                    <li><a href="<?php echo BASE_URL();?>home">Home</a></li>
                    <li class="active">Contact Us</li>
                </ol>
            </div>
        </section>
        <section class="content content--fill content--fill--light top-null bottom-null fullwidth">
            <h2 class="text-center lined">Contact Us</h2>
            <div id="map"></div>
        </section>
        <section class="content content--fill content--fill--light top-null">
            <div class="container">
                <div class="row">
                    <div class="col-sm-1 visible-lg"></div>
                    <div class="col-sm-3">
                        <div class="address">
                            <div class="pull-right"><img src="<?php echo BASE_URL();?>assets/images/logo.png" class="img-responsive" alt="" /></div>
                            <div class="divider divider--sm"></div>
                            <div class="text-right">
                                <h6>Our address:</h6> <?php echo SITE_STREET;?>
                                <br>   <?php echo SITE_CITY;?> , <?php echo SITE_STATE;?>
                                <br> <?php echo SITE_COUNTRY;?> </div>
                            <div class="divider divider--sm"></div>
                            <div class="text-right">
                                <h6>Call us:</h6><?php echo SITE_NUMBER;?> </div>
                            <div class="divider divider--sm"></div>
                            <div class="text-right">
                                <h6>Have any questions?</h6> <a href="mailto:<?php echo SITE_EMAIL;?>"><?php echo SITE_EMAIL;?></a>
                                <br>  </div>
                            <div class="divider divider--md"></div>
                            <div class="social-links lg">
                                <ul>
                                    <!--<li>
                                        <a class="icon icon-twitter-logo-button" href="#"></a>
                                    </li>-->
                                    <li>
                                        <a class="icon icon-facebook-logo-button" href="#"></a>
                                    </li>
                                   <!-- <li>
                                        <a class="icon icon-instagram-logo" href="#"></a>
                                    </li>-->
                                </ul>
                                <ul>
                                   <!-- <li>
                                        <a class="icon icon-google-plus" href="#"></a>
                                    </li>
                                    <li>
                                        <a class="icon icon-social" href="#"></a>
                                    </li>
                                    <li>
                                        <a class="icon icon-linkedin-logo-button" href="#"></a>
                                    </li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1 visible-lg"></div>
                    <div class="col-sm-7">
                        <form id="contactform" class="contact-form" name="contactform" method="post" novalidate>
                            <div id="success">
                                <p>Your message was sent successfully!</p>
                            </div>
                            <div id="error">
                                <p>Something went wrong, try refreshing and submitting the form again.</p>
                            </div>
                            <div class="inputs-col">
                                <div class="input-wrapper">
                                    <label>Name <span class="required">*</span></label>
                                    <input type="text" class="input--wd input--full" name="name" placeholder="Your name"> </div>
                                <div class="input-wrapper">
                                    <label>Phone</label>
                                    <input type="text" class="input--wd input--full" name="phone" placeholder="(555) 555-5555"> </div>
                                <div class="input-wrapper">
                                    <label>Email</label>
                                    <input type="text" class="input--wd input--full" name="email" placeholder="xxxx@xxxx.xxx"> </div>
                            </div>
                            <div>
                                <label>Message</label>
                                <textarea class="textarea--wd input--full" name="message"></textarea>
                            </div>
                            <div class="divider divider--xs"></div>
                            <button type="submit" id="submit" class="btn btn--wd">Send Message</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <div class="page-bot"><img src="<?php echo BASE_URL();?>assets/images/under-footer.png" class="img-responsive" alt=""></div>
    </div>
    <?php include "includes/footer.php";?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAP_API_KEY; ?>&callback=initMap"></script>
    <script type="text/javascript">

        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            var mapOptions = {
                zoom: 14
                , scrollwheel: false
                ,
                center: new google.maps.LatLng(<?php echo LATLNG;?>),

                styles: [{
                    "featureType": "water"
                    , "elementType": "geometry"
                    , "stylers": [{
                        "color": "#e9e9e9"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "landscape"
                    , "elementType": "geometry"
                    , "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "road.highway"
                    , "elementType": "geometry.fill"
                    , "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "road.highway"
                    , "elementType": "geometry.stroke"
                    , "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 29
                    }, {
                        "weight": 0.2
                    }]
                }, {
                    "featureType": "road.arterial"
                    , "elementType": "geometry"
                    , "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 18
                    }]
                }, {
                    "featureType": "road.local"
                    , "elementType": "geometry"
                    , "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "poi"
                    , "elementType": "geometry"
                    , "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "featureType": "poi.park"
                    , "elementType": "geometry"
                    , "stylers": [{
                        "color": "#dedede"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "elementType": "labels.text.stroke"
                    , "stylers": [{
                        "visibility": "on"
                    }, {
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "elementType": "labels.text.fill"
                    , "stylers": [{
                        "saturation": 36
                    }, {
                        "color": "#333333"
                    }, {
                        "lightness": 40
                    }]
                }, {
                    "elementType": "labels.icon"
                    , "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "transit"
                    , "elementType": "geometry"
                    , "stylers": [{
                        "color": "#f2f2f2"
                    }, {
                        "lightness": 19
                    }]
                }, {
                    "featureType": "administrative"
                    , "elementType": "geometry.fill"
                    , "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "administrative"
                    , "elementType": "geometry.stroke"
                    , "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 17
                    }, {
                        "weight": 1.2
                    }]
                }]
            };

            var mapElement = document.getElementById('map');

            var map = new google.maps.Map(mapElement, mapOptions);
            var image = 'https://lh3.googleusercontent.com/-8PyfdDLP-_g/WMGabVb8zBI/AAAAAAAABoM/qujFkg8N9CYvrBedSphsXfXxX1-_lWKaQCL0B/h143/2017-03-09.png';

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(<?php echo LATLNG;?>)
                , map: map
                , icon: image
            });
        }

    </script>
    </body>
    </html>