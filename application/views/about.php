<?php include "includes/header.php";?>
<div class="wrapper">
    <?php include "includes/nav-bar.php";?>
    <div id="pageContent" class="page-content">
        <section class="breadcrumbs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
                    <li><a href="<?php echo BASE_URL();?>home">Home</a></li>
                    <li class="active">About Us</li>
                </ol>
            </div>
        </section>
        <section class="content content--fill content--fill--light top-null">
            <div class="container">
                <h1 class="text-center lined">About Us</h1>
                <p class="text-center info-text">Starting out with just a single truck and mower, we have expanded our services and grown into one of the largest lawn maintenance companies in our area. Our expansion and stellar reputation is due, in part, to our exceptional reputation for quality and timely service. Our lawn care technicians utilize the latest technology and techniques to deliver beautiful results that will stand the test of time.</p>
                <div class="row">
                    <div class="col-sm-6 animation" data-animation="fadeInLeft" data-animation-delay="0.5s">
                        <div class="text-icon__title">Our Mission</div>
                        <div class="text-icon last">
                            <div class="text-icon__icon"><i class="icon icon-nature-2"></i></div>
                            <div class="text-icon__info">
                                <p>Our mission is to provide our customers with the highest level of quality services. We pledge to establish lasting relationships with our clients by exceeding their expectations and gaining their trust through exceptional performance. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 animation" data-animation="fadeInRight" data-animation-delay="0.5s">
                        <div class="text-icon__title">Our Clients</div>
                        <div class="text-icon last">
                            <div class="text-icon__icon"><i class="icon icon-construction-worker"></i></div>
                            <div class="text-icon__info">
                                <p>Our clients count on our dependability, our drive, and our integrity and we take great pride in our accomplishments and build on them every day.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider divider--md"></div>
                <h2 class="text-center lined">Our Advantages</h2>
                <div class="row">
                    <div class="col-sm-4"><img src="<?php echo BASE_URL();?>assets/images/img-about-1.jpg" class="img-responsive" alt=""></div>
                    <div class="col-sm-4"><img src="<?php echo BASE_URL();?>assets/images/img-about-2.jpg" class="img-responsive" alt=""></div>
                    <div class="col-sm-4"><img src="<?php echo BASE_URL();?>assets/images/img-about-3.jpg" class="img-responsive" alt=""></div>
                </div>
                <div class="divider divider--md"></div>
                <div class="text-center">
                    <p>We are a full service landscape maintenance company who commit to fulfilling all your landscape and lawn related needs. Our team of experts have over 28 years of experience in the landscape industry, which helps us develop and deliver custom made programs and solutions for you and your lawn. Through constant communication between our lawn care, landscape maintenance, and irrigation managers, we are able to provide you with a solution for any landscape. Our maintenance team completes a full property analysis during each visit and resolves any landscape related issues immediately to ensure your lawn is at its healthiest and looks its best. Lawn Care provides professional and quality landscaping services for both residential and commercial properties. From start to finish, we offer a wide range of lawn care services to accommodate your needs every step along the way.</p>
                    <p>We take pride in providing customers satisfaction that is second-to-none. Our team of professionals is fully licensed, insured, and well trained to provide you with consistent and high-quality value and services.</p>
                </div>
                <div class="divider divider--md"></div>
                <h2 class="text-center lined">Lawn Care Team</h2>
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="person animation" data-animation="fadeInLeft" data-animation-delay="0.3s">
                            <div class="person__image"><img src="<?php echo BASE_URL();?>assets/images/person-01.jpg" alt="" class="img-responsive" /></div>
                            <div class="person__title">Mark Ronson</div>
                            <div class="person__position">Customer Service Manager</div>
                            <div class="person__text">
                                <p>He joined our team 3 years ago and we are excited to have him as our lead technician</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="person animation" data-animation="fadeInLeft" data-animation-delay="0s">
                            <div class="person__image"><img src="<?php echo BASE_URL();?>assets/images/person-02.jpg" alt="" class="img-responsive" /></div>
                            <div class="person__title">Christopher Stoudt</div>
                            <div class="person__position">Fertilizer Technician</div>
                            <div class="person__text">
                                <p>Over 6 years of experience in the lawn care industry and an interest in organic solutions</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="person animation" data-animation="fadeInRight" data-animation-delay="0s">
                            <div class="person__image"><img src="<?php echo BASE_URL();?>assets/images/person-03.jpg" alt="" class="img-responsive" /></div>
                            <div class="person__title">Joe Saboe</div>
                            <div class="person__position">Technician</div>
                            <div class="person__text">
                                <p>He has worked as a certified technician for the past 7 years</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="person animation last" data-animation="fadeInRight" data-animation-delay="0.3s">
                            <div class="person__image"><img src="<?php echo BASE_URL();?>assets/images/person-04.jpg" alt="" class="img-responsive" /></div>
                            <div class="person__title">Alisa Madden</div>
                            <div class="person__position">Administrative Assistant</div>
                            <div class="person__text">
                                <p>She is always available for customer's needs and answers the phone with a smile</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider divider--md"></div>
                <div class="text-center"><a class="btn btn--wd">MEET ALL TEAM</a></div>
            </div>
        </section>
        <div class="page-bot"><img src="<?php echo BASE_URL();?>assets/images/under-footer.png" class="img-responsive" alt=""></div>
    </div>
    <?php include "includes/footer.php";?>
    </body>
    </html>